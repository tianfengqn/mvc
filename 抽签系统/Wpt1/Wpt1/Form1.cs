﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wpt1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //方法
        public int[] getRandomNum(int num, int minValue, int maxValue)
        {
            Random ra = new Random(unchecked((int)DateTime.Now.Ticks));
            int[] arrNum = new int[num];
            int tmp = 0;
            for (int i = 0; i <= num - 1; i++)
            {
                tmp = ra.Next(minValue, maxValue); //随机取数
                arrNum[i] = getNum(arrNum, tmp, minValue, maxValue, ra); //取出值赋到数组中
            }
            return arrNum;
        }

        public int getNum(int[] arrNum, int tmp, int minValue, int maxValue, Random ra)  //嵌套调用
        {
            foreach (int item in arrNum)
            {
                if (item == tmp) //利用循环判断是否有重复
                {
                    tmp = ra.Next(minValue, maxValue); //重新随机获取。
                    tmp = getNum(arrNum, tmp, minValue, maxValue, ra);//递归:如果取出来的数字和已取得的数字有重复就重新随机获取。
                }
            }
            return tmp;
        }

        //窗体运行
        private void Form1_Load(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
        }
        //确定按钮
        private void button3_Click(object sender, EventArgs e)
        {
            int startnum = Convert.ToInt32(textBox2.Text);
            int endnum = Convert.ToInt32(textBox4.Text);
            int count = Convert.ToInt32(textBox3.Text);
            string result1 = "";
            //string result2 = "";
            if (startnum >= endnum)
            {
                MessageBox.Show("起始编号必须小于结束编号");
            }
            else if (count > endnum - startnum)
            {
                MessageBox.Show("抽取数量不可大于可抽取数");
            }
            else if (count > 1000)
            {
                result1 = "抽取数量不要超过1000";
            }
            groupBox1.Visible = false;
        }

        private void btnks_Click(object sender, EventArgs e)
        {
            int startnum = Convert.ToInt32(textBox2.Text);
            int endnum = Convert.ToInt32(textBox4.Text);
            int count = Convert.ToInt32(textBox3.Text);
            string result1 = "";
            //string result2 = "";
            if (startnum >= endnum)
            {
                textEdit1.Text = "起始编号必须小于结束编号";
            }
            else if (count > endnum - startnum)
            {
                textEdit1.Text = "抽取数量不可大于可抽取数";
            }
            else if (count > 1000)
            {
                textEdit1.Text = "抽取数量不要超过1000";
            }
            else
            {
                int[] result = getRandomNum(count, startnum, endnum);
                result1 = string.Join(",", result);
                textEdit1.Text = result1;

            }
        }

        private void btnsz_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
        }
    }
}
