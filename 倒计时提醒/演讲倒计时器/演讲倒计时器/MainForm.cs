﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using System.Media;


namespace 演讲倒计时器
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        public MainForm()
        {
            InitializeComponent();
        }
        TimeSpan ts = new TimeSpan(0, 0, 0);
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            string timestr = timeSpanEdit1.EditValue.ToString();
            string[] timelong = timestr.Split(':');
            timer1.Enabled = true;
            ts = new TimeSpan(Convert.ToInt32(timelong[0]), Convert.ToInt32(timelong[1]), Convert.ToInt32(timelong[2]));
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            string str = string.Format("{0}:{1}:{2}", ts.Hours.ToString("##00"), ts.Minutes.ToString("00"), ts.Seconds.ToString("00"));
            lblTime.Text = str;//label17用来显示剩余的时间
            ts = ts.Subtract(new TimeSpan(0, 0, 1));//每隔一秒减去一秒
            if(ts.TotalSeconds < 0.0)//当倒计时完毕
            {
                timer1.Enabled = false;
                SoundPlayer player = new SoundPlayer(演讲倒计时器.Properties.Resources._329);
                //player.SoundLocation = "音乐文件名";
                player.Load();
                player.Play();
                MessageBox.Show("时间到！~~~");//提示时间到,下面可以加你想要的操作
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            pnlSet.Visible = true;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            lblTitle.Text = txtTitle.EditValue.ToString();
            lblTime.Text = timeSpanEdit1.EditValue.ToString();
            pnlSet.Visible = false;
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            lblTitle.Text = txtTitle.EditValue.ToString();
            lblTime.Text = timeSpanEdit1.EditValue.ToString();

        }

    }
}