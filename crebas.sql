/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     2017/6/11 19:19:20                           */
/*==============================================================*/


if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.OperaLog')
            and   type = 'U')
   drop table dbo.OperaLog
go

alter table dbo.OperaRole
   drop constraint PK_Role
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.OperaRole')
            and   type = 'U')
   drop table dbo.OperaRole
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.UserRole')
            and   type = 'U')
   drop table dbo.UserRole
go

alter table dbo.[101]
   drop constraint PK_101
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[101]')
            and   type = 'U')
   drop table dbo.[101]
go

alter table dbo.[102]
   drop constraint Pk_Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[102]')
            and   type = 'U')
   drop table dbo.[102]
go

alter table dbo.[104]
   drop constraint Pk_104Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[104]')
            and   type = 'U')
   drop table dbo.[104]
go

alter table dbo.[106]
   drop constraint Pk_106Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[106]')
            and   type = 'U')
   drop table dbo.[106]
go

alter table dbo.[107]
   drop constraint Pk_107Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[107]')
            and   type = 'U')
   drop table dbo.[107]
go

alter table dbo.[109]
   drop constraint Pk_109Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[109]')
            and   type = 'U')
   drop table dbo.[109]
go

alter table dbo.[110]
   drop constraint Pk_110Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[110]')
            and   type = 'U')
   drop table dbo.[110]
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[111]')
            and   type = 'U')
   drop table dbo.[111]
go

alter table dbo.[113]
   drop constraint Pk_113Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[113]')
            and   type = 'U')
   drop table dbo.[113]
go

alter table dbo.[114]
   drop constraint Pk_114Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[114]')
            and   type = 'U')
   drop table dbo.[114]
go

alter table dbo.[115]
   drop constraint Pk_115Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[115]')
            and   type = 'U')
   drop table dbo.[115]
go

alter table dbo.[116]
   drop constraint Pk_116Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[116]')
            and   type = 'U')
   drop table dbo.[116]
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[201]')
            and   type = 'U')
   drop table dbo.[201]
go

alter table dbo.[202]
   drop constraint Pk_202Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[202]')
            and   type = 'U')
   drop table dbo.[202]
go

alter table dbo.[203]
   drop constraint Pk_203Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[203]')
            and   type = 'U')
   drop table dbo.[203]
go

alter table dbo.[205]
   drop constraint Pk_205Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[205]')
            and   type = 'U')
   drop table dbo.[205]
go

alter table dbo.[206]
   drop constraint Pk_206Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[206]')
            and   type = 'U')
   drop table dbo.[206]
go

alter table dbo.[207]
   drop constraint Pk_207Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[207]')
            and   type = 'U')
   drop table dbo.[207]
go

alter table dbo.[301]
   drop constraint Pk_301Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[301]')
            and   type = 'U')
   drop table dbo.[301]
go

alter table dbo.[401]
   drop constraint Pk_401Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[401]')
            and   type = 'U')
   drop table dbo.[401]
go

alter table dbo.[402]
   drop constraint Pk_402Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[402]')
            and   type = 'U')
   drop table dbo.[402]
go

alter table dbo.[403]
   drop constraint Pk_403Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[403]')
            and   type = 'U')
   drop table dbo.[403]
go

alter table dbo.[404]
   drop constraint Pk_404Id
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.[404]')
            and   type = 'U')
   drop table dbo.[404]
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.bkb')
            and   name  = 'Ix_Bkb_KmmxidKmdm'
            and   indid > 0
            and   indid < 255)
   drop index dbo.bkb.Ix_Bkb_KmmxidKmdm
go

alter table dbo.bkb
   drop constraint PK_BKB
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.bkb')
            and   type = 'U')
   drop table dbo.bkb
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.djhtlxb')
            and   type = 'U')
   drop table dbo.djhtlxb
go

alter table dbo.dwdmb
   drop constraint PK_dwdmb
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.dwdmb')
            and   type = 'U')
   drop table dbo.dwdmb
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.dyb')
            and   type = 'U')
   drop table dbo.dyb
go

if exists (select 1
            from  sysobjects
           where  id = object_id('fjb')
            and   type = 'U')
   drop table fjb
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('dbo.hkb')
            and   name  = 'Ix_Hkb_KmmxidKmdm'
            and   indid > 0
            and   indid < 255)
   drop index dbo.hkb.Ix_Hkb_KmmxidKmdm
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.hkb')
            and   type = 'U')
   drop table dbo.hkb
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.jkb')
            and   type = 'U')
   drop table dbo.jkb
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.kmb')
            and   type = 'U')
   drop table dbo.kmb
go

if exists (select 1
            from  sysobjects
           where  id = object_id('lxyjb')
            and   type = 'U')
   drop table lxyjb
go

alter table dbo.shqrjk
   drop constraint PK_SHQRJK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.shqrjk')
            and   type = 'U')
   drop table dbo.shqrjk
go

alter table tzb
   drop constraint PK_TZB
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tzb')
            and   type = 'U')
   drop table tzb
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.yhb')
            and   type = 'U')
   drop table dbo.yhb
go

alter table dbo.yhdkhkjhb
   drop constraint PK_yhdkhkjhb
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.yhdkhkjhb')
            and   type = 'U')
   drop table dbo.yhdkhkjhb
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.zbbData')
            and   type = 'U')
   drop table dbo.zbbData
go

/*==============================================================*/
/* Table: OperaLog                                              */
/*==============================================================*/
create table dbo.OperaLog (
   Logid                int                  identity(1, 1),
   UserCode             nvarchar(6)          collate Chinese_PRC_CI_AS null,
   LoginIp              varchar(15)          collate Chinese_PRC_CI_AS null,
   OperationTable       nchar(10)            collate Chinese_PRC_CI_AS null,
   Tableid              int                  null,
   OperationType        int                  null,
   Operation            varchar(1000)        collate Chinese_PRC_CI_AS null,
   OperationDate        smalldatetime        null constraint DF_Table_1_ActionDate default getdate()
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.OperaLog') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'OperaLog' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'OperaLog操作日志', 
   'user', 'dbo', 'table', 'OperaLog'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Logid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaLog', 'column', 'Logid'

end


execute sp_addextendedproperty 'MS_Description', 
   'Logid',
   'user', 'dbo', 'table', 'OperaLog', 'column', 'Logid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UserCode')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaLog', 'column', 'UserCode'

end


execute sp_addextendedproperty 'MS_Description', 
   'UserCode',
   'user', 'dbo', 'table', 'OperaLog', 'column', 'UserCode'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'LoginIp')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaLog', 'column', 'LoginIp'

end


execute sp_addextendedproperty 'MS_Description', 
   '登陆ip',
   'user', 'dbo', 'table', 'OperaLog', 'column', 'LoginIp'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'OperationTable')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaLog', 'column', 'OperationTable'

end


execute sp_addextendedproperty 'MS_Description', 
   'OperationTable',
   'user', 'dbo', 'table', 'OperaLog', 'column', 'OperationTable'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Tableid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaLog', 'column', 'Tableid'

end


execute sp_addextendedproperty 'MS_Description', 
   'Tableid',
   'user', 'dbo', 'table', 'OperaLog', 'column', 'Tableid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'OperationType')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaLog', 'column', 'OperationType'

end


execute sp_addextendedproperty 'MS_Description', 
   '操作类型',
   'user', 'dbo', 'table', 'OperaLog', 'column', 'OperationType'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Operation')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaLog', 'column', 'Operation'

end


execute sp_addextendedproperty 'MS_Description', 
   '操作内容',
   'user', 'dbo', 'table', 'OperaLog', 'column', 'Operation'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'OperationDate')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaLog', 'column', 'OperationDate'

end


execute sp_addextendedproperty 'MS_Description', 
   '操作日期',
   'user', 'dbo', 'table', 'OperaLog', 'column', 'OperationDate'
go

/*==============================================================*/
/* Table: OperaRole                                             */
/*==============================================================*/
create table dbo.OperaRole (
   RoleID               int                  identity(1, 1),
   Usercode             nvarchar(6)          collate Chinese_PRC_CI_AS null,
   TableName            nchar(10)            collate Chinese_PRC_CI_AS null,
   JBQkAdd              bit                  null constraint DF_Role_JBQkAdd default (0),
   JbQkUpdate           bit                  null constraint DF_Role_JbQkUpdate default (0),
   JBQkDelete           bit                  null constraint DF_Role_JBQkDelete default (0),
   BfkAdd               bit                  null constraint DF_Role_BfkAdd default (0),
   BfkUpdate            bit                  null constraint DF_Role_BfkUpdate default (0),
   BfkDelete            bit                  null constraint DF_Role_BfkDelete default (0),
   QrLAdd               bit                  null constraint DF_Role_QrLAdd default (0),
   QrlUpdate            bit                  null constraint DF_Role_QrlUpdate default (0),
   QrlDelete            bit                  null constraint DF_Role_QrlDelete default (0),
   JhAdd                bit                  null constraint DF__OperaRole__JhAdd__6641052B default (0),
   JhUpdate             bit                  null constraint DF__OperaRole__JhUpd__67352964 default (0),
   JhDelete             bit                  null constraint DF__OperaRole__JhDel__68294D9D default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.OperaRole') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'OperaRole' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'OperaRole用户权限分配/科目操作权限', 
   'user', 'dbo', 'table', 'OperaRole'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'RoleID')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'RoleID'

end


execute sp_addextendedproperty 'MS_Description', 
   'RoleID',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'RoleID'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Usercode')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'Usercode'

end


execute sp_addextendedproperty 'MS_Description', 
   'Usercode',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'Usercode'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'TableName')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'TableName'

end


execute sp_addextendedproperty 'MS_Description', 
   'TableName',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'TableName'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JBQkAdd')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JBQkAdd'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目增',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JBQkAdd'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JbQkUpdate')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JbQkUpdate'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目改',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JbQkUpdate'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JBQkDelete')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JBQkDelete'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目删',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JBQkDelete'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'BfkAdd')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'BfkAdd'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款增',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'BfkAdd'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'BfkUpdate')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'BfkUpdate'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款改',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'BfkUpdate'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'BfkDelete')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'BfkDelete'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款删',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'BfkDelete'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'QrLAdd')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'QrLAdd'

end


execute sp_addextendedproperty 'MS_Description', 
   '确认类增',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'QrLAdd'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'QrlUpdate')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'QrlUpdate'

end


execute sp_addextendedproperty 'MS_Description', 
   '确认类改',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'QrlUpdate'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'QrlDelete')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'QrlDelete'

end


execute sp_addextendedproperty 'MS_Description', 
   '确认类删',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'QrlDelete'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JhAdd')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JhAdd'

end


execute sp_addextendedproperty 'MS_Description', 
   '计划类增',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JhAdd'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JhUpdate')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JhUpdate'

end


execute sp_addextendedproperty 'MS_Description', 
   '计划类改',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JhUpdate'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.OperaRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JhDelete')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JhDelete'

end


execute sp_addextendedproperty 'MS_Description', 
   '计划类删',
   'user', 'dbo', 'table', 'OperaRole', 'column', 'JhDelete'
go

alter table dbo.OperaRole
   add constraint PK_Role primary key (RoleID)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: UserRole                                              */
/*==============================================================*/
create table dbo.UserRole (
   userCode             nvarchar(6)          collate Chinese_PRC_CI_AS null,
   RoleCode             int                  null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.UserRole') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'UserRole' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'UserRole', 
   'user', 'dbo', 'table', 'UserRole'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.UserRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'userCode')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'UserRole', 'column', 'userCode'

end


execute sp_addextendedproperty 'MS_Description', 
   'userCode',
   'user', 'dbo', 'table', 'UserRole', 'column', 'userCode'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.UserRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'RoleCode')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'UserRole', 'column', 'RoleCode'

end


execute sp_addextendedproperty 'MS_Description', 
   'RoleCode',
   'user', 'dbo', 'table', 'UserRole', 'column', 'RoleCode'
go

/*==============================================================*/
/* Table: [101]                                                 */
/*==============================================================*/
create table dbo.[101] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   htbh                 char(15)             collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(50)         collate Chinese_PRC_CI_AS not null,
   xmwz                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   lxyj                 nvarchar(20)         collate Chinese_PRC_CI_AS null,
   sfztb                nvarchar(5)          collate Chinese_PRC_CI_AS null,
   jsdw                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   jsdwlxr              nvarchar(4)          collate Chinese_PRC_CI_AS null,
   jsdwlxdh             nvarchar(11)         collate Chinese_PRC_CI_AS null,
   sgdw                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   sgdwlxr              nvarchar(4)          collate Chinese_PRC_CI_AS null,
   sgdwlxdh             nvarchar(11)         collate Chinese_PRC_CI_AS null,
   htj                  money                null constraint DF_101_htj default (0),
   qrhje                money                null constraint DF_101_qrhje default (0),
   tzcs                 smallint             null,
   jexj                 money                null constraint DF_101_jexj default (0),
   bkcs                 smallint             null,
   htsh                 bit                  null constraint DF_101_htsh default (0),
   yfje                 money                null constraint DF_101_yfje default (0.0),
   sjsqk                money                null constraint DF_101_htsqk default (0.0),
   sqk                  money                null constraint DF_101_sqk default (0.0),
   beiz                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null constraint DF_101_Nbkxj default (0),
   Ybkxj                money                null constraint DF_101_Ybkxj default (0),
   Ljbkxj               money                null constraint DF_101_Ljbkxj default (0),
   sjj                  bit                  null constraint DF__101__sjj__18027DF1 default (0),
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__101__lrsj__2FE4F47A default getdate(),
   flag                 int                  null constraint DF__101__flag__30D918B3 default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   SjPDFid              char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   qtlyzjje             char(10)             null,
   qtlyzily             char(10)             null,
   sjyzfje              char(10)             null,
   xmnd                 char(10)             null,
   kgsj                 char(10)             null,
   jgsj                 char(10)             null,
   fkfs                 char(10)             null,
   zbfs                 char(10)             null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[101]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[101]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '101工程项目', 
   'user', 'dbo', 'table', '[101]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[101]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[101]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[101]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[101]', 'column', 'xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[101]', 'column', 'xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'htbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'htbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '合同编号',
   'user', 'dbo', 'table', '[101]', 'column', 'htbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[101]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmwz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'xmwz'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目位置',
   'user', 'dbo', 'table', '[101]', 'column', 'xmwz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxyj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'lxyj'

end


execute sp_addextendedproperty 'MS_Description', 
   '立项依据',
   'user', 'dbo', 'table', '[101]', 'column', 'lxyj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sfztb')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'sfztb'

end


execute sp_addextendedproperty 'MS_Description', 
   'sfztb',
   'user', 'dbo', 'table', '[101]', 'column', 'sfztb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jsdw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'jsdw'

end


execute sp_addextendedproperty 'MS_Description', 
   '甲方（建设单位）',
   'user', 'dbo', 'table', '[101]', 'column', 'jsdw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jsdwlxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'jsdwlxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '甲方联系人',
   'user', 'dbo', 'table', '[101]', 'column', 'jsdwlxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jsdwlxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'jsdwlxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '甲方联系电话',
   'user', 'dbo', 'table', '[101]', 'column', 'jsdwlxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sgdw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'sgdw'

end


execute sp_addextendedproperty 'MS_Description', 
   '乙方（施工单位）',
   'user', 'dbo', 'table', '[101]', 'column', 'sgdw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sgdwlxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'sgdwlxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '乙方联系人',
   'user', 'dbo', 'table', '[101]', 'column', 'sgdwlxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sgdwlxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'sgdwlxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '乙方联系人电话',
   'user', 'dbo', 'table', '[101]', 'column', 'sgdwlxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'htj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'htj'

end


execute sp_addextendedproperty 'MS_Description', 
   '合同价',
   'user', 'dbo', 'table', '[101]', 'column', 'htj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '经审核金额',
   'user', 'dbo', 'table', '[101]', 'column', 'qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'tzcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'tzcs',
   'user', 'dbo', 'table', '[101]', 'column', 'tzcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   '小计',
   'user', 'dbo', 'table', '[101]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[101]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'htsh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'htsh'

end


execute sp_addextendedproperty 'MS_Description', 
   '施工合同审核',
   'user', 'dbo', 'table', '[101]', 'column', 'htsh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付金额',
   'user', 'dbo', 'table', '[101]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'sjsqk',
   'user', 'dbo', 'table', '[101]', 'column', 'sjsqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款（合同）',
   'user', 'dbo', 'table', '[101]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[101]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[101]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[101]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目办付款',
   'user', 'dbo', 'table', '[101]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sjj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'sjj'

end


execute sp_addextendedproperty 'MS_Description', 
   '审计价',
   'user', 'dbo', 'table', '[101]', 'column', 'sjj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[101]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[101]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[101]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[101]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[101]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'SjPDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'SjPDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'SjPDFid',
   'user', 'dbo', 'table', '[101]', 'column', 'SjPDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   'FlagReason',
   'user', 'dbo', 'table', '[101]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[101]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[101]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'qtlyzjje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'qtlyzjje'

end


execute sp_addextendedproperty 'MS_Description', 
   '其他来源资金金额',
   'user', 'dbo', 'table', '[101]', 'column', 'qtlyzjje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'qtlyzily')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'qtlyzily'

end


execute sp_addextendedproperty 'MS_Description', 
   '其他来源资金来源',
   'user', 'dbo', 'table', '[101]', 'column', 'qtlyzily'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sjyzfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'sjyzfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '实际应支付金额',
   'user', 'dbo', 'table', '[101]', 'column', 'sjyzfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmnd')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'xmnd'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目年度',
   'user', 'dbo', 'table', '[101]', 'column', 'xmnd'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'kgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '开工时间',
   'user', 'dbo', 'table', '[101]', 'column', 'kgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'jgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '竣工时间',
   'user', 'dbo', 'table', '[101]', 'column', 'jgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fkfs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'fkfs'

end


execute sp_addextendedproperty 'MS_Description', 
   '付款方式（一次付款，分期付款，其他）',
   'user', 'dbo', 'table', '[101]', 'column', 'fkfs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[101]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'zbfs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[101]', 'column', 'zbfs'

end


execute sp_addextendedproperty 'MS_Description', 
   '招标方式（公开招标，邀请招标，竞争性谈判，单一来源采购，询价，直接发包，其他）',
   'user', 'dbo', 'table', '[101]', 'column', 'zbfs'
go

alter table dbo.[101]
   add constraint PK_101 primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [102]                                                 */
/*==============================================================*/
create table dbo.[102] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   zyfw                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   mj                   real                 null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   tdbck                money                null constraint DF_102_tdbck default (0),
   azbcf                money                null constraint DF_102_azbcf default (0),
   qmf                  money                null constraint DF_102_qmf default (0),
   qita                 money                null constraint DF_102_qita default (0),
   jexj                 money                null constraint DF_102_jexj default (0),
   bkcs                 smallint             null constraint DF_102_bkcs default (0),
   yfje                 money                null constraint DF__102__yfje__1DE57479 default (0.0),
   dhmx                 bit                  not null constraint DF_102_dhmx default (0),
   sqk                  money                null constraint DF_102_sqk default (0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null constraint DF_102_Nbkxj default (0),
   Ybkxj                money                null constraint DF_102_Ybkxj default (0),
   Ljbkxj               money                null constraint DF_102_Ljbkxj default (0),
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__102__lrsj__31CD3CEC default getdate(),
   flag                 int                  null constraint DF__102__flag__32C16125 default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Qrhje                money                null constraint DF__102__Qrhje__0D25C822 default (0),
   Sjsqk                money                null constraint DF__102__Sjsqk__0E19EC5B default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[102]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[102]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '102征地补偿款', 
   'user', 'dbo', 'table', '[102]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[102]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[102]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称xmmc',
   'user', 'dbo', 'table', '[102]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'zyfw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'zyfw'

end


execute sp_addextendedproperty 'MS_Description', 
   'zyfw',
   'user', 'dbo', 'table', '[102]', 'column', 'zyfw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'mj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'mj'

end


execute sp_addextendedproperty 'MS_Description', 
   '面积（亩）',
   'user', 'dbo', 'table', '[102]', 'column', 'mj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[102]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '立项人',
   'user', 'dbo', 'table', '[102]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   'lxdh',
   'user', 'dbo', 'table', '[102]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tdbck')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'tdbck'

end


execute sp_addextendedproperty 'MS_Description', 
   '土地补偿款',
   'user', 'dbo', 'table', '[102]', 'column', 'tdbck'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'azbcf')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'azbcf'

end


execute sp_addextendedproperty 'MS_Description', 
   '安置补偿费',
   'user', 'dbo', 'table', '[102]', 'column', 'azbcf'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'qmf')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'qmf'

end


execute sp_addextendedproperty 'MS_Description', 
   'qmf',
   'user', 'dbo', 'table', '[102]', 'column', 'qmf'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'qita')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'qita'

end


execute sp_addextendedproperty 'MS_Description', 
   '其他',
   'user', 'dbo', 'table', '[102]', 'column', 'qita'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jexj',
   'user', 'dbo', 'table', '[102]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[102]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   'yfje',
   'user', 'dbo', 'table', '[102]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dhmx')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'dhmx'

end


execute sp_addextendedproperty 'MS_Description', 
   'dhmx',
   'user', 'dbo', 'table', '[102]', 'column', 'dhmx'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'sqk',
   'user', 'dbo', 'table', '[102]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   'beiz',
   'user', 'dbo', 'table', '[102]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[102]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[102]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[102]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[102]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[102]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[102]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[102]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[102]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[102]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[102]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[102]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[102]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   'Xmxl',
   'user', 'dbo', 'table', '[102]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   'Xmzl',
   'user', 'dbo', 'table', '[102]', 'column', 'Xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'Qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   'Qrhje',
   'user', 'dbo', 'table', '[102]', 'column', 'Qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[102]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[102]', 'column', 'Sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'Sjsqk',
   'user', 'dbo', 'table', '[102]', 'column', 'Sjsqk'
go

alter table dbo.[102]
   add constraint Pk_Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [104]                                                 */
/*==============================================================*/
create table dbo.[104] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   zyfw                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   mj                   real                 null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   jexj                 money                null constraint DF_104_jexj default (0),
   bkcs                 smallint             null constraint DF_104_bkcs default (0),
   dhmx                 bit                  not null constraint DF_104_dhmx default (0),
   yfje                 money                null constraint DF_104_yfje default (0),
   sqk                  money                null constraint DF_104_sqk default (0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null constraint DF_104_Nbkxj default (0),
   Ybkxj                money                null constraint DF_104_Ybkxj default (0),
   Ljbkxj               money                null constraint DF_104_Ljbkxj default (0),
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__104__lrsj__359DCDD0 default getdate(),
   flag                 int                  null constraint DF__104__flag__3691F209 default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Qrhje                money                null constraint DF__104__Qrhje__10F65906 default (0),
   Sjsqk                money                null constraint DF__104__Sjsqk__11EA7D3F default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[104]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[104]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '104树木、附属物等补偿款', 
   'user', 'dbo', 'table', '[104]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[104]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[104]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[104]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'zyfw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'zyfw'

end


execute sp_addextendedproperty 'MS_Description', 
   '征用范围',
   'user', 'dbo', 'table', '[104]', 'column', 'zyfw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'mj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'mj'

end


execute sp_addextendedproperty 'MS_Description', 
   '面积（亩）',
   'user', 'dbo', 'table', '[104]', 'column', 'mj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[104]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[104]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[104]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jexj',
   'user', 'dbo', 'table', '[104]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款次数',
   'user', 'dbo', 'table', '[104]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dhmx')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'dhmx'

end


execute sp_addextendedproperty 'MS_Description', 
   '到户明细',
   'user', 'dbo', 'table', '[104]', 'column', 'dhmx'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付金额',
   'user', 'dbo', 'table', '[104]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[104]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[104]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[104]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[104]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[104]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[104]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[104]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[104]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[104]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[104]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   '合同附件（PDF）',
   'user', 'dbo', 'table', '[104]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[104]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[104]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[104]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[104]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[104]', 'column', 'Xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'Qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '经审核金额',
   'user', 'dbo', 'table', '[104]', 'column', 'Qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[104]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[104]', 'column', 'Sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '实际尚欠款',
   'user', 'dbo', 'table', '[104]', 'column', 'Sjsqk'
go

alter table dbo.[104]
   add constraint Pk_104Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [106]                                                 */
/*==============================================================*/
create table dbo.[106] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   zyfw                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   mj                   real                 null,
   jm                   smallint             null,
   ckqy                 smallint             null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   cqbck                money                null constraint DF_106_cqbck default (0),
   gzjf                 money                null constraint DF_106_gzjf default (0),
   qita                 money                null constraint DF_106_qita default (0),
   jexj                 money                null constraint DF_106_jexj default (0),
   bkcs                 smallint             null constraint DF_106_bkcs default (0),
   sfjz                 bit                  not null constraint DF_106_sfjz default (0),
   yfje                 money                null constraint DF_106_yfje default (0),
   sqk                  money                null constraint DF_106_sqk default (0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null constraint DF_106_Nbkxj default (0),
   Ybkxj                money                null constraint DF_106_Ybkxj default (0),
   Ljbkxj               money                null constraint DF_106_Ljbkxj default (0),
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__106__lrsj__396E5EB4 default getdate(),
   flag                 int                  null constraint DF__106__flag__3A6282ED default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Qrhje                money                null constraint DF__106__Qrhje__14C6E9EA default (0),
   Sjsqk                money                null constraint DF__106__Sjsqk__15BB0E23 default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[106]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[106]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '106拆迁补偿款', 
   'user', 'dbo', 'table', '[106]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[106]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[106]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[106]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'zyfw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'zyfw'

end


execute sp_addextendedproperty 'MS_Description', 
   '拆迁范围',
   'user', 'dbo', 'table', '[106]', 'column', 'zyfw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'mj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'mj'

end


execute sp_addextendedproperty 'MS_Description', 
   '面积（亩）',
   'user', 'dbo', 'table', '[106]', 'column', 'mj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'jm'

end


execute sp_addextendedproperty 'MS_Description', 
   '居民（户）',
   'user', 'dbo', 'table', '[106]', 'column', 'jm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ckqy')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'ckqy'

end


execute sp_addextendedproperty 'MS_Description', 
   'ckqy',
   'user', 'dbo', 'table', '[106]', 'column', 'ckqy'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[106]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[106]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[106]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'cqbck')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'cqbck'

end


execute sp_addextendedproperty 'MS_Description', 
   'cqbck',
   'user', 'dbo', 'table', '[106]', 'column', 'cqbck'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'gzjf')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'gzjf'

end


execute sp_addextendedproperty 'MS_Description', 
   'gzjf',
   'user', 'dbo', 'table', '[106]', 'column', 'gzjf'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'qita')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'qita'

end


execute sp_addextendedproperty 'MS_Description', 
   '其他',
   'user', 'dbo', 'table', '[106]', 'column', 'qita'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   '金额小计',
   'user', 'dbo', 'table', '[106]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[106]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sfjz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'sfjz'

end


execute sp_addextendedproperty 'MS_Description', 
   'sfjz',
   'user', 'dbo', 'table', '[106]', 'column', 'sfjz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付款',
   'user', 'dbo', 'table', '[106]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[106]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[106]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[106]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[106]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[106]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[106]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[106]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[106]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[106]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[106]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[106]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[106]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[106]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[106]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[106]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[106]', 'column', 'Xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'Qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '经审核金额',
   'user', 'dbo', 'table', '[106]', 'column', 'Qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[106]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[106]', 'column', 'Sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'Sjsqk',
   'user', 'dbo', 'table', '[106]', 'column', 'Sjsqk'
go

alter table dbo.[106]
   add constraint Pk_106Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [107]                                                 */
/*==============================================================*/
create table dbo.[107] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmwz                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   hgts                 smallint             null,
   hgmj                 real                 null,
   zzts                 smallint             null,
   zzmj                 real                 null,
   zzdj                 money                null,
   spts                 smallint             null,
   spmj                 real                 null,
   spdj                 money                null,
   htj                  money                null,
   tzcs                 smallint             null,
   tzj1                 money                null,
   tzsy1                nvarchar(50)         collate Chinese_PRC_CI_AS null,
   tzj2                 money                null,
   tzsy2                nvarchar(50)         collate Chinese_PRC_CI_AS null,
   tzj3                 money                null,
   tzsy3                nvarchar(50)         collate Chinese_PRC_CI_AS null,
   tzj4                 money                null,
   tzsy4                nvarchar(50)         collate Chinese_PRC_CI_AS null,
   tzj5                 money                null,
   tzsy5                nvarchar(50)         collate Chinese_PRC_CI_AS null,
   tzj6                 money                null,
   tzsy6                nvarchar(50)         collate Chinese_PRC_CI_AS null,
   jexj                 money                null,
   bkcs                 smallint             null,
   yfje                 money                null,
   sqk                  money                null,
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null,
   Ybkxj                money                null,
   Ljbkxj               money                null,
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__107__lrsj__3B56A726 default getdate(),
   flag                 int                  null constraint DF__107__flag__3C4ACB5F default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Qrhje                money                null constraint DF__107__Qrhje__16AF325C default (0),
   Sjsqk                money                null constraint DF__107__Sjsqk__17A35695 default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[107]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[107]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '107回购房款', 
   'user', 'dbo', 'table', '[107]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[107]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[107]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[107]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmwz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'xmwz'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目位置',
   'user', 'dbo', 'table', '[107]', 'column', 'xmwz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[107]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[107]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[107]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hgts')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'hgts'

end


execute sp_addextendedproperty 'MS_Description', 
   '回购套数',
   'user', 'dbo', 'table', '[107]', 'column', 'hgts'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hgmj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'hgmj'

end


execute sp_addextendedproperty 'MS_Description', 
   '回购面积',
   'user', 'dbo', 'table', '[107]', 'column', 'hgmj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'zzts')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'zzts'

end


execute sp_addextendedproperty 'MS_Description', 
   'zzts',
   'user', 'dbo', 'table', '[107]', 'column', 'zzts'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'zzmj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'zzmj'

end


execute sp_addextendedproperty 'MS_Description', 
   '住宅面积',
   'user', 'dbo', 'table', '[107]', 'column', 'zzmj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'zzdj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'zzdj'

end


execute sp_addextendedproperty 'MS_Description', 
   '住宅单价',
   'user', 'dbo', 'table', '[107]', 'column', 'zzdj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'spts')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'spts'

end


execute sp_addextendedproperty 'MS_Description', 
   '商铺套数',
   'user', 'dbo', 'table', '[107]', 'column', 'spts'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'spmj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'spmj'

end


execute sp_addextendedproperty 'MS_Description', 
   '商铺面积',
   'user', 'dbo', 'table', '[107]', 'column', 'spmj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'spdj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'spdj'

end


execute sp_addextendedproperty 'MS_Description', 
   '商铺单价',
   'user', 'dbo', 'table', '[107]', 'column', 'spdj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'htj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'htj'

end


execute sp_addextendedproperty 'MS_Description', 
   '合同价',
   'user', 'dbo', 'table', '[107]', 'column', 'htj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzcs'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整次数',
   'user', 'dbo', 'table', '[107]', 'column', 'tzcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzj1')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzj1'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整金额1',
   'user', 'dbo', 'table', '[107]', 'column', 'tzj1'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzsy1')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy1'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整事由1',
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy1'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzj2')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzj2'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整金额2',
   'user', 'dbo', 'table', '[107]', 'column', 'tzj2'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzsy2')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy2'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整事由2',
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy2'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzj3')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzj3'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整金额3',
   'user', 'dbo', 'table', '[107]', 'column', 'tzj3'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzsy3')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy3'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整事由3',
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy3'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzj4')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzj4'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整金额4',
   'user', 'dbo', 'table', '[107]', 'column', 'tzj4'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzsy4')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy4'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整事由4',
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy4'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzj5')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzj5'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整金额5',
   'user', 'dbo', 'table', '[107]', 'column', 'tzj5'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzsy5')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy5'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整事由5',
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy5'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzj6')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzj6'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整金额6',
   'user', 'dbo', 'table', '[107]', 'column', 'tzj6'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzsy6')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy6'

end


execute sp_addextendedproperty 'MS_Description', 
   '调整事由6',
   'user', 'dbo', 'table', '[107]', 'column', 'tzsy6'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   '小计',
   'user', 'dbo', 'table', '[107]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[107]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付金额',
   'user', 'dbo', 'table', '[107]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款（合同）',
   'user', 'dbo', 'table', '[107]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[107]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[107]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[107]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[107]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[107]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[107]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[107]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[107]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[107]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[107]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[107]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[107]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[107]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[107]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[107]', 'column', 'Xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'Qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '经审核金额',
   'user', 'dbo', 'table', '[107]', 'column', 'Qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[107]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[107]', 'column', 'Sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'Sjsqk',
   'user', 'dbo', 'table', '[107]', 'column', 'Sjsqk'
go

alter table dbo.[107]
   add constraint Pk_107Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [109]                                                 */
/*==============================================================*/
create table dbo.[109] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   dkzt                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   dkyh                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   dkxm                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   bkcs                 smallint             null constraint DF_109_bkcs default (0),
   jexj                 money                null constraint DF_109_jexj default (0.0),
   yfje                 money                null constraint DF_109_yfje default (0.0),
   sqk                  money                null constraint DF_109_sqk default (0.0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null constraint DF_109_Nbkxj default (0),
   Ybkxj                money                null constraint DF_109_Ybkxj default (0),
   Ljbkxj               money                null constraint DF_109_Ljbkxj default (0),
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__109__lrsj__3F27380A default getdate(),
   flag                 int                  null constraint DF__109__flag__401B5C43 default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Qrhje                money                null constraint DF__109__Qrhje__306F045F default (0),
   Sjsqk                money                null constraint DF__109__Sjsqk__31632898 default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[109]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[109]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '109融资利息', 
   'user', 'dbo', 'table', '[109]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[109]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[109]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dkzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'dkzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '贷款主体',
   'user', 'dbo', 'table', '[109]', 'column', 'dkzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dkyh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'dkyh'

end


execute sp_addextendedproperty 'MS_Description', 
   '贷款银行',
   'user', 'dbo', 'table', '[109]', 'column', 'dkyh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dkxm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'dkxm'

end


execute sp_addextendedproperty 'MS_Description', 
   '贷款项目',
   'user', 'dbo', 'table', '[109]', 'column', 'dkxm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[109]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jexj',
   'user', 'dbo', 'table', '[109]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付款',
   'user', 'dbo', 'table', '[109]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[109]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[109]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[109]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[109]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[109]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[109]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[109]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[109]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[109]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[109]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[109]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[109]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[109]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[109]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[109]', 'column', 'Xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[109]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'Qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '经审核金额',
   'user', 'dbo', 'table', '[109]', 'column', 'Qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[109]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[109]', 'column', 'Sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'Sjsqk',
   'user', 'dbo', 'table', '[109]', 'column', 'Sjsqk'
go

alter table dbo.[109]
   add constraint Pk_109Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [110]                                                 */
/*==============================================================*/
create table dbo.[110] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmwz                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   jexj                 money                null constraint DF_110_jexj default (0),
   bkcs                 smallint             null constraint DF_110_bkcs default (0),
   yfje                 money                null constraint DF_110_yfje default (0),
   sqk                  money                null constraint DF_110_sqk default (0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null constraint DF_110_Nbkxj default (0),
   Ybkxj                money                null constraint DF_110_Ybkxj default (0),
   Ljbkxj               money                null constraint DF_110_Ljbkxj default (0),
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__110__lrsj__410F807C default getdate(),
   flag                 int                  null constraint DF__110__flag__4203A4B5 default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Qrhje                money                null constraint DF__110__Qrhje__1A7FC340 default (0),
   Sjsqk                money                null constraint DF__110__Sjsqk__1B73E779 default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[110]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[110]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '110企业改制相关费用', 
   'user', 'dbo', 'table', '[110]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[110]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[110]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[110]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmwz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'xmwz'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目位置',
   'user', 'dbo', 'table', '[110]', 'column', 'xmwz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[110]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[110]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[110]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jexj',
   'user', 'dbo', 'table', '[110]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[110]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付款',
   'user', 'dbo', 'table', '[110]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[110]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[110]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[110]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[110]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[110]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[110]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[110]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[110]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[110]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[110]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[110]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[110]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[110]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[110]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[110]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[110]', 'column', 'Xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'Qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '经审核金额',
   'user', 'dbo', 'table', '[110]', 'column', 'Qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[110]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[110]', 'column', 'Sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'Sjsqk',
   'user', 'dbo', 'table', '[110]', 'column', 'Sjsqk'
go

alter table dbo.[110]
   add constraint Pk_110Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [111]                                                 */
/*==============================================================*/
create table dbo.[111] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   dkxmbh               varchar(11)          collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   jexj                 money                null,
   bkcs                 smallint             null,
   yfje                 money                null,
   sqk                  money                null,
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null,
   Ybkxj                money                null,
   Ljbkxj               money                null,
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__111__lrsj__0955373E default getdate(),
   flag                 int                  null constraint DF__111__flag__0A495B77 default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[111]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[111]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '111融资成本', 
   'user', 'dbo', 'table', '[111]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[111]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[111]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[111]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[111]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dkxmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'dkxmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '贷款项目编号',
   'user', 'dbo', 'table', '[111]', 'column', 'dkxmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[111]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[111]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   '金额小计',
   'user', 'dbo', 'table', '[111]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[111]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付款',
   'user', 'dbo', 'table', '[111]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[111]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[111]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[111]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[111]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[111]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[111]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[111]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[111]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[111]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[111]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[111]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[111]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[111]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[111]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[111]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[111]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[111]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[111]', 'column', 'Xmzl'
go

/*==============================================================*/
/* Table: [113]                                                 */
/*==============================================================*/
create table dbo.[113] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmwz                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   jexj                 money                null constraint DF_113_jexj default (0),
   bkcs                 smallint             null constraint DF_113_bkcs default (0),
   yfje                 money                null constraint DF_113_yfje default (0),
   sqk                  money                null constraint DF_113_sqk default (0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null constraint DF_113_Nbkxj default (0),
   Ybkxj                money                null constraint DF_113_Ybkxj default (0),
   Ljbkxj               money                null constraint DF_113_Ljbkxj default (0),
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__113__lrsj__44E01160 default getdate(),
   flag                 int                  null constraint DF__113__flag__45D43599 default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Qrhje                money                null constraint DF__113__Qrhje__1E505424 default (0),
   Sjsqk                money                null constraint DF__113__Sjsqk__1F44785D default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[113]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[113]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '113拆迁延期过渡费', 
   'user', 'dbo', 'table', '[113]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[113]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[113]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[113]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmwz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'xmwz'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目位置',
   'user', 'dbo', 'table', '[113]', 'column', 'xmwz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[113]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[113]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[113]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jexj',
   'user', 'dbo', 'table', '[113]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[113]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付款',
   'user', 'dbo', 'table', '[113]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[113]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[113]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[113]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[113]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[113]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[113]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[113]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[113]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[113]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[113]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[113]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[113]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[113]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[113]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[113]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[113]', 'column', 'Xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'Qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '经审核金额',
   'user', 'dbo', 'table', '[113]', 'column', 'Qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[113]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[113]', 'column', 'Sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'Sjsqk',
   'user', 'dbo', 'table', '[113]', 'column', 'Sjsqk'
go

alter table dbo.[113]
   add constraint Pk_113Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [114]                                                 */
/*==============================================================*/
create table dbo.[114] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmwz                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   jexj                 money                null constraint DF_114_jexj default (0),
   bkcs                 smallint             null constraint DF_114_bkcs default (0),
   yfje                 money                null constraint DF_114_yfje default (0),
   sqk                  money                null constraint DF_114_sqk default (0),
   beiz                 nvarchar(Max)        collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null constraint DF_114_Nbkxj default (0),
   Ybkxj                money                null constraint DF_114_Ybkxj default (0),
   Ljbkxj               money                null constraint DF_114_Ljbkxj default (0),
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__114__lrsj__46C859D2 default getdate(),
   flag                 int                  null constraint DF__114__flag__47BC7E0B default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Qrhje                money                null constraint DF__114__Qrhje__20389C96 default (0),
   Sjsqk                money                null constraint DF__114__Sjsqk__212CC0CF default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[114]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[114]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '114其他1', 
   'user', 'dbo', 'table', '[114]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[114]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[114]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[114]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmwz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'xmwz'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目位置',
   'user', 'dbo', 'table', '[114]', 'column', 'xmwz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[114]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[114]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人电话',
   'user', 'dbo', 'table', '[114]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jexj',
   'user', 'dbo', 'table', '[114]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[114]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付款',
   'user', 'dbo', 'table', '[114]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[114]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[114]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[114]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[114]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[114]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[114]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[114]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[114]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[114]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[114]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[114]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[114]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[114]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[114]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[114]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[114]', 'column', 'Xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'Qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '经审核金额',
   'user', 'dbo', 'table', '[114]', 'column', 'Qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[114]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[114]', 'column', 'Sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'Sjsqk',
   'user', 'dbo', 'table', '[114]', 'column', 'Sjsqk'
go

alter table dbo.[114]
   add constraint Pk_114Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [115]                                                 */
/*==============================================================*/
create table dbo.[115] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmwz                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   jexj                 money                null constraint DF_115_jexj default (0),
   bkcs                 smallint             null constraint DF_115_bkcs default (0),
   yfje                 money                null constraint DF_115_yfje default (0),
   sqk                  money                null constraint DF_115_sqk default (0),
   beiz                 nvarchar(Max)        collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null constraint DF_115_Nbkxj default (0),
   Ybkxj                money                null constraint DF_115_Ybkxj default (0),
   Ljbkxj               money                null constraint DF_115_Ljbkxj default (0),
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__115__lrsj__48B0A244 default getdate(),
   flag                 int                  null constraint DF__115__flag__49A4C67D default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Qrhje                money                null constraint DF__115__Qrhje__2220E508 default (0),
   Sjsqk                money                null constraint DF__115__Sjsqk__23150941 default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[115]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[115]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '115其他支出（预算', 
   'user', 'dbo', 'table', '[115]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[115]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[115]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[115]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmwz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'xmwz'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目位置',
   'user', 'dbo', 'table', '[115]', 'column', 'xmwz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[115]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[115]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[115]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jexj',
   'user', 'dbo', 'table', '[115]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[115]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付款',
   'user', 'dbo', 'table', '[115]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[115]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[115]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[115]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[115]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[115]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[115]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[115]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[115]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[115]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[115]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[115]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[115]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[115]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[115]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[115]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[115]', 'column', 'Xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'Qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '经审核金额',
   'user', 'dbo', 'table', '[115]', 'column', 'Qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[115]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[115]', 'column', 'Sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'Sjsqk',
   'user', 'dbo', 'table', '[115]', 'column', 'Sjsqk'
go

alter table dbo.[115]
   add constraint Pk_115Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [116]                                                 */
/*==============================================================*/
create table dbo.[116] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmwz                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   jexj                 money                null constraint DF_116_jexj default (0),
   bkcs                 smallint             null constraint DF_116_bkcs default (0),
   yfje                 money                null constraint DF_116_yfje default (0),
   sqk                  money                null constraint DF_116_sqk default (0),
   beiz                 nvarchar(Max)        collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null constraint DF_116_Nbkxj default (0),
   Ybkxj                money                null constraint DF_116_Ybkxj default (0),
   Ljbkxj               money                null constraint DF_116_Ljbkxj default (0),
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__116__lrsj__4A98EAB6 default getdate(),
   flag                 int                  null constraint DF__116__flag__4B8D0EEF default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Xmxl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Xmzl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   Qrhje                money                null constraint DF__116__Qrhje__24092D7A default (0),
   Sjsqk                money                null constraint DF__116__Sjsqk__24FD51B3 default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[116]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[116]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '116其他支出（空转', 
   'user', 'dbo', 'table', '[116]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[116]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[116]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[116]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmwz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'xmwz'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目位置',
   'user', 'dbo', 'table', '[116]', 'column', 'xmwz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[116]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[116]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[116]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jexj',
   'user', 'dbo', 'table', '[116]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'bkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bkcs',
   'user', 'dbo', 'table', '[116]', 'column', 'bkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付款',
   'user', 'dbo', 'table', '[116]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[116]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[116]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[116]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[116]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[116]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目大类',
   'user', 'dbo', 'table', '[116]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[116]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[116]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[116]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[116]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[116]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记原因',
   'user', 'dbo', 'table', '[116]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', '[116]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[116]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmxl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'Xmxl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目小类',
   'user', 'dbo', 'table', '[116]', 'column', 'Xmxl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Xmzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'Xmzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目子类',
   'user', 'dbo', 'table', '[116]', 'column', 'Xmzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Qrhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'Qrhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '经审核金额',
   'user', 'dbo', 'table', '[116]', 'column', 'Qrhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[116]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sjsqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[116]', 'column', 'Sjsqk'

end


execute sp_addextendedproperty 'MS_Description', 
   'Sjsqk',
   'user', 'dbo', 'table', '[116]', 'column', 'Sjsqk'
go

alter table dbo.[116]
   add constraint Pk_116Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [201]                                                 */
/*==============================================================*/
create table dbo.[201] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   dkxz                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   dkbh                 nvarchar(20)         collate Chinese_PRC_CI_AS null,
   kfs                  nvarchar(40)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(11)         collate Chinese_PRC_CI_AS null,
   tdzl                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   mj                   real                 null,
   htj                  money                null,
   cjsj                 datetime             null,
   ydjkje1              money                null,
   ydjksj1              datetime             null,
   ydjkje2              money                null,
   ydjksj2              datetime             null,
   ydjkje3              money                null,
   ydjksj3              datetime             null,
   ydjkje4              money                null,
   ydjksj4              datetime             null,
   ydjkje5              money                null,
   ydjksj5              datetime             null,
   ydjkje6              money                null,
   ydjksj6              datetime             null,
   jexj                 money                null,
   jkcs                 smallint             null,
   yfje                 money                null,
   gytdsyjj             money                null,
   lytdkfjj             money                null,
   tdcrzjk              money                null,
   tdcrywf              money                null,
   czlzfbzjj            money                null,
   jyzj                 money                null,
   slzj                 money                null,
   sqk                  money                null,
   bhcs                 smallint             null,
   ybhje                money                null,
   wbhje                money                null,
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   Nbkxj                money                null,
   Ybkxj                money                null,
   Ljbkxj               money                null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null,
   flag                 int                  null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   Htzt                 bit                  null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[201]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[201]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '201土地出让金', 
   'user', 'dbo', 'table', '[201]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[201]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', '[201]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dkxz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'dkxz'

end


execute sp_addextendedproperty 'MS_Description', 
   '地块性质',
   'user', 'dbo', 'table', '[201]', 'column', 'dkxz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dkbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'dkbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '地块编号',
   'user', 'dbo', 'table', '[201]', 'column', 'dkbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kfs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'kfs'

end


execute sp_addextendedproperty 'MS_Description', 
   '受让单位',
   'user', 'dbo', 'table', '[201]', 'column', 'kfs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[201]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[201]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tdzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'tdzl'

end


execute sp_addextendedproperty 'MS_Description', 
   '地块位置',
   'user', 'dbo', 'table', '[201]', 'column', 'tdzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'mj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'mj'

end


execute sp_addextendedproperty 'MS_Description', 
   '面积',
   'user', 'dbo', 'table', '[201]', 'column', 'mj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'htj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'htj'

end


execute sp_addextendedproperty 'MS_Description', 
   '成交价',
   'user', 'dbo', 'table', '[201]', 'column', 'htj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'cjsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'cjsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '成交时间',
   'user', 'dbo', 'table', '[201]', 'column', 'cjsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjkje1')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje1'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjkje1',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje1'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjksj1')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj1'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjksj1',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj1'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjkje2')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje2'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjkje2',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje2'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjksj2')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj2'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjksj2',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj2'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjkje3')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje3'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjkje3',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje3'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjksj3')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj3'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjksj3',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj3'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjkje4')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje4'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjkje4',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje4'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjksj4')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj4'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjksj4',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj4'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjkje5')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje5'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjkje5',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje5'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjksj5')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj5'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjksj5',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj5'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjkje6')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje6'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjkje6',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjkje6'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydjksj6')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj6'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydjksj6',
   'user', 'dbo', 'table', '[201]', 'column', 'ydjksj6'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jexj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'jexj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jexj',
   'user', 'dbo', 'table', '[201]', 'column', 'jexj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'jkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款次数',
   'user', 'dbo', 'table', '[201]', 'column', 'jkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已缴款',
   'user', 'dbo', 'table', '[201]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'gytdsyjj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'gytdsyjj'

end


execute sp_addextendedproperty 'MS_Description', 
   'gytdsyjj',
   'user', 'dbo', 'table', '[201]', 'column', 'gytdsyjj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lytdkfjj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'lytdkfjj'

end


execute sp_addextendedproperty 'MS_Description', 
   'lytdkfjj',
   'user', 'dbo', 'table', '[201]', 'column', 'lytdkfjj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tdcrzjk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'tdcrzjk'

end


execute sp_addextendedproperty 'MS_Description', 
   'tdcrzjk',
   'user', 'dbo', 'table', '[201]', 'column', 'tdcrzjk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tdcrywf')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'tdcrywf'

end


execute sp_addextendedproperty 'MS_Description', 
   'tdcrywf',
   'user', 'dbo', 'table', '[201]', 'column', 'tdcrywf'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'czlzfbzjj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'czlzfbzjj'

end


execute sp_addextendedproperty 'MS_Description', 
   'czlzfbzjj',
   'user', 'dbo', 'table', '[201]', 'column', 'czlzfbzjj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jyzj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'jyzj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jyzj',
   'user', 'dbo', 'table', '[201]', 'column', 'jyzj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'slzj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'slzj'

end


execute sp_addextendedproperty 'MS_Description', 
   'slzj',
   'user', 'dbo', 'table', '[201]', 'column', 'slzj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[201]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bhcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'bhcs'

end


execute sp_addextendedproperty 'MS_Description', 
   'bhcs',
   'user', 'dbo', 'table', '[201]', 'column', 'bhcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ybhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'ybhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已拨回金额',
   'user', 'dbo', 'table', '[201]', 'column', 'ybhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'wbhje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'wbhje'

end


execute sp_addextendedproperty 'MS_Description', 
   '未拨回金额',
   'user', 'dbo', 'table', '[201]', 'column', 'wbhje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[201]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', '[201]', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'Ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ybkxj',
   'user', 'dbo', 'table', '[201]', 'column', 'Ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'Ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Ljbkxj',
   'user', 'dbo', 'table', '[201]', 'column', 'Ljbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[201]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[201]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[201]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   'flag',
   'user', 'dbo', 'table', '[201]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', '[201]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[201]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   'FlagReason',
   'user', 'dbo', 'table', '[201]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[201]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Htzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[201]', 'column', 'Htzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[201]', 'column', 'Htzt'
go

/*==============================================================*/
/* Table: [202]                                                 */
/*==============================================================*/
create table dbo.[202] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   yfje                 money                null constraint DF_202_yfje default (0),
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   sj                   datetime             null,
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__202__lrsj__4E697B9A default getdate(),
   flag                 int                  null constraint DF__202__flag__4F5D9FD3 default (0),
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   PzN                  varchar(4)           collate Chinese_PRC_CI_AS null,
   PzY                  varchar(2)           collate Chinese_PRC_CI_AS null,
   Pzbh                 varchar(3)           collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null constraint DF__202__xmzt__25F175EC default '',
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 varchar(11)          collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[202]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[202]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '202财政预算内拨款', 
   'user', 'dbo', 'table', '[202]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[202]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[202]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '金额',
   'user', 'dbo', 'table', '[202]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[202]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'sj'

end


execute sp_addextendedproperty 'MS_Description', 
   '时间',
   'user', 'dbo', 'table', '[202]', 'column', 'sj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[202]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目类型',
   'user', 'dbo', 'table', '[202]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[202]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[202]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[202]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[202]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', '[202]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[202]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PzN')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'PzN'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证(年)',
   'user', 'dbo', 'table', '[202]', 'column', 'PzN'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PzY')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'PzY'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证(月)',
   'user', 'dbo', 'table', '[202]', 'column', 'PzY'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Pzbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'Pzbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证编号',
   'user', 'dbo', 'table', '[202]', 'column', 'Pzbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[202]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[202]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[202]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[202]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[202]', 'column', 'lxdh'
go

alter table dbo.[202]
   add constraint Pk_202Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [203]                                                 */
/*==============================================================*/
create table dbo.[203] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   yfje                 money                null constraint DF_203_yfje default (0),
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   sj                   datetime             null,
   beiz                 nvarchar(Max)        collate Chinese_PRC_CI_AS null,
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__203__lrsj__5051C40C default getdate(),
   flag                 int                  null constraint DF__203__flag__5145E845 default (0),
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   PzN                  varchar(4)           collate Chinese_PRC_CI_AS null,
   PzY                  varchar(2)           collate Chinese_PRC_CI_AS null,
   Pzbh                 varchar(3)           collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null constraint DF__203__xmzt__26E59A25 default '',
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 varchar(11)          collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[203]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[203]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '203盐化工基础设施补助款', 
   'user', 'dbo', 'table', '[203]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[203]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[203]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '金额',
   'user', 'dbo', 'table', '[203]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[203]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'sj'

end


execute sp_addextendedproperty 'MS_Description', 
   '时间',
   'user', 'dbo', 'table', '[203]', 'column', 'sj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[203]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目类型',
   'user', 'dbo', 'table', '[203]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[203]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[203]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[203]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[203]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', '[203]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[203]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PzN')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'PzN'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证(年)',
   'user', 'dbo', 'table', '[203]', 'column', 'PzN'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PzY')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'PzY'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证(月)',
   'user', 'dbo', 'table', '[203]', 'column', 'PzY'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Pzbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'Pzbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证编号',
   'user', 'dbo', 'table', '[203]', 'column', 'Pzbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[203]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[203]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[203]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[203]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[203]', 'column', 'lxdh'
go

alter table dbo.[203]
   add constraint Pk_203Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [205]                                                 */
/*==============================================================*/
create table dbo.[205] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   yfje                 money                null constraint DF_205_yfje default (0.0),
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   sj                   datetime             null,
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__205__lrsj__542254F0 default getdate(),
   flag                 int                  null constraint DF__205__flag__55167929 default (0),
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   PzN                  varchar(4)           collate Chinese_PRC_CI_AS null,
   PzY                  varchar(2)           collate Chinese_PRC_CI_AS null,
   Pzbh                 varchar(3)           collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null constraint DF__205__xmzt__27D9BE5E default '',
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 varchar(11)          collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[205]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[205]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '205选房差价款', 
   'user', 'dbo', 'table', '[205]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[205]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', '[205]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '金额',
   'user', 'dbo', 'table', '[205]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[205]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'sj'

end


execute sp_addextendedproperty 'MS_Description', 
   '时间',
   'user', 'dbo', 'table', '[205]', 'column', 'sj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[205]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目类型',
   'user', 'dbo', 'table', '[205]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[205]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[205]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[205]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[205]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', '[205]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[205]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PzN')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'PzN'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证(年)',
   'user', 'dbo', 'table', '[205]', 'column', 'PzN'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PzY')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'PzY'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证(月)',
   'user', 'dbo', 'table', '[205]', 'column', 'PzY'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Pzbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'Pzbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证编号',
   'user', 'dbo', 'table', '[205]', 'column', 'Pzbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[205]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[205]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[205]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[205]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[205]', 'column', 'lxdh'
go

alter table dbo.[205]
   add constraint Pk_205Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [206]                                                 */
/*==============================================================*/
create table dbo.[206] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   yfje                 money                null constraint DF_206_yfje default (0),
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   sj                   datetime             null,
   beiz                 nvarchar(Max)        collate Chinese_PRC_CI_AS null,
   xmdl                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__206__lrsj__560A9D62 default getdate(),
   flag                 int                  null constraint DF__206__flag__56FEC19B default (0),
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   PzN                  varchar(4)           collate Chinese_PRC_CI_AS null,
   PzY                  varchar(2)           collate Chinese_PRC_CI_AS null,
   Pzbh                 varchar(3)           collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null constraint DF__206__xmzt__28CDE297 default '',
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 varchar(11)          collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[206]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[206]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '206配套资金', 
   'user', 'dbo', 'table', '[206]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[206]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', '[206]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '金额',
   'user', 'dbo', 'table', '[206]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[206]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'sj'

end


execute sp_addextendedproperty 'MS_Description', 
   '时间',
   'user', 'dbo', 'table', '[206]', 'column', 'sj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[206]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目类型',
   'user', 'dbo', 'table', '[206]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[206]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[206]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[206]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[206]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', '[206]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[206]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PzN')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'PzN'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证（年）',
   'user', 'dbo', 'table', '[206]', 'column', 'PzN'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PzY')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'PzY'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证（月）',
   'user', 'dbo', 'table', '[206]', 'column', 'PzY'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Pzbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'Pzbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证编号',
   'user', 'dbo', 'table', '[206]', 'column', 'Pzbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[206]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[206]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[206]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[206]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[206]', 'column', 'lxdh'
go

alter table dbo.[206]
   add constraint Pk_206Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [207]                                                 */
/*==============================================================*/
create table dbo.[207] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   yfje                 money                null constraint DF_207_yfje default (0),
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   sj                   datetime             null,
   beiz                 nvarchar(Max)        collate Chinese_PRC_CI_AS null,
   xmdl                 nvarchar(50)         collate Chinese_PRC_CI_AS null,
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__207__lrsj__57F2E5D4 default getdate(),
   flag                 int                  null constraint DF__207__flag__58E70A0D default (0),
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   PzN                  varchar(4)           collate Chinese_PRC_CI_AS null,
   PzY                  varchar(2)           collate Chinese_PRC_CI_AS null,
   Pzbh                 varchar(3)           collate Chinese_PRC_CI_AS null,
   xmzt                 nvarchar(30)         collate Chinese_PRC_CI_AS null constraint DF__207__xmzt__29C206D0 default '',
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 varchar(11)          collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[207]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[207]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '207其他', 
   'user', 'dbo', 'table', '[207]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[207]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', '[207]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '金额',
   'user', 'dbo', 'table', '[207]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[207]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'sj'

end


execute sp_addextendedproperty 'MS_Description', 
   '时间',
   'user', 'dbo', 'table', '[207]', 'column', 'sj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[207]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmdl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'xmdl'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目类型',
   'user', 'dbo', 'table', '[207]', 'column', 'xmdl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[207]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[207]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[207]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[207]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', '[207]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[207]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PzN')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'PzN'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证（年）',
   'user', 'dbo', 'table', '[207]', 'column', 'PzN'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PzY')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'PzY'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证（月）',
   'user', 'dbo', 'table', '[207]', 'column', 'PzY'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Pzbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'Pzbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证编号',
   'user', 'dbo', 'table', '[207]', 'column', 'Pzbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'xmzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目主体',
   'user', 'dbo', 'table', '[207]', 'column', 'xmzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[207]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[207]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[207]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[207]', 'column', 'lxdh'
go

alter table dbo.[207]
   add constraint Pk_207Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [301]                                                 */
/*==============================================================*/
create table dbo.[301] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   jkdw                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   lxr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lxdh                 nvarchar(50)         collate Chinese_PRC_CI_AS null,
   jkje                 money                null constraint DF_301_jkje default (0),
   yfje                 money                null constraint DF_301_yfje default (0),
   sqk                  money                null constraint DF_301_sqk default (0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   yhkxj                money                null constraint DF_301_yhkxj default (0),
   nhkxj                money                null constraint DF_301_nhkxj default (0),
   ljhkxj               money                null constraint DF_301_ljhkxj default (0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[301]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[301]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '301其他应收款', 
   'user', 'dbo', 'table', '[301]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[301]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', '[301]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkdw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'jkdw'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款单位',
   'user', 'dbo', 'table', '[301]', 'column', 'jkdw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'lxr'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', 'dbo', 'table', '[301]', 'column', 'lxr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxdh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'lxdh'

end


execute sp_addextendedproperty 'MS_Description', 
   '联系电话',
   'user', 'dbo', 'table', '[301]', 'column', 'lxdh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'jkje'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款金额（元）',
   'user', 'dbo', 'table', '[301]', 'column', 'jkje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已还金额（元）',
   'user', 'dbo', 'table', '[301]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款（元）',
   'user', 'dbo', 'table', '[301]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[301]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'yhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'yhkxj',
   'user', 'dbo', 'table', '[301]', 'column', 'yhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'nhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'nhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'nhkxj',
   'user', 'dbo', 'table', '[301]', 'column', 'nhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[301]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ljhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[301]', 'column', 'ljhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'ljhkxj',
   'user', 'dbo', 'table', '[301]', 'column', 'ljhkxj'
go

alter table dbo.[301]
   add constraint Pk_301Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [401]                                                 */
/*==============================================================*/
create table dbo.[401] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   jkr                  nvarchar(40)         collate Chinese_PRC_CI_AS null,
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   dkyh                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   htqdrq               datetime             null,
   dkje                 money                null constraint DF_401_dkje default (0),
   lilv                 real                 null constraint DF_401_lilv default (0),
   qix                  real                 null constraint DF_401_qix default (0),
   bxhj                 money                null constraint DF_401_bxhj default (0),
   ydhksj               datetime             null,
   fkcs                 int                  null constraint DF_401_fkcs default (0),
   fkje1                money                null,
   fksj1                datetime             null,
   fkje2                money                null,
   fksj2                datetime             null,
   fkje3                money                null,
   fksj3                datetime             null,
   fkje4                money                null,
   fksj4                datetime             null,
   fkje5                money                null,
   fksj5                datetime             null,
   dzxj                 money                null,
   sfdiya               bit                  not null constraint DF_401_sfdiya default (0),
   hkcs                 smallint             null constraint DF_401_hkcs default (0),
   yfje                 money                null constraint DF_401_yfje default (0),
   sqk                  money                null constraint DF_401_sqk default (0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   yhkxj                money                null constraint DF_401_yhkxj default (0),
   nhkxj                money                null constraint DF_401_nhkxj default (0),
   ljhkxj               money                null constraint DF_401_ljhkxj default (0),
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__401__lrsj__5BC376B8 default getdate(),
   flag                 int                  null constraint DF__401__flag__5CB79AF1 default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   Zrdw                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   Zrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   Dklx                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   FlagReason           nvarchar(50)         collate Chinese_PRC_CI_AS null,
   Rzcb                 money                null constraint DF__401__Rzcb__465E457E default (0.0)
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[401]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[401]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '401银行贷款', 
   'user', 'dbo', 'table', '[401]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[401]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', '[401]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'jkr'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款人',
   'user', 'dbo', 'table', '[401]', 'column', 'jkr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[401]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dkyh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'dkyh'

end


execute sp_addextendedproperty 'MS_Description', 
   '贷款银行',
   'user', 'dbo', 'table', '[401]', 'column', 'dkyh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'htqdrq')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'htqdrq'

end


execute sp_addextendedproperty 'MS_Description', 
   '合同签订',
   'user', 'dbo', 'table', '[401]', 'column', 'htqdrq'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dkje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'dkje'

end


execute sp_addextendedproperty 'MS_Description', 
   '贷款金额',
   'user', 'dbo', 'table', '[401]', 'column', 'dkje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lilv')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'lilv'

end


execute sp_addextendedproperty 'MS_Description', 
   '年利率（%）',
   'user', 'dbo', 'table', '[401]', 'column', 'lilv'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'qix')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'qix'

end


execute sp_addextendedproperty 'MS_Description', 
   '期限（月)',
   'user', 'dbo', 'table', '[401]', 'column', 'qix'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bxhj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'bxhj'

end


execute sp_addextendedproperty 'MS_Description', 
   'bxhj',
   'user', 'dbo', 'table', '[401]', 'column', 'bxhj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydhksj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'ydhksj'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydhksj',
   'user', 'dbo', 'table', '[401]', 'column', 'ydhksj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   '放款次数',
   'user', 'dbo', 'table', '[401]', 'column', 'fkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fkje1')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fkje1'

end


execute sp_addextendedproperty 'MS_Description', 
   '放款金额(元)',
   'user', 'dbo', 'table', '[401]', 'column', 'fkje1'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fksj1')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fksj1'

end


execute sp_addextendedproperty 'MS_Description', 
   'fksj1',
   'user', 'dbo', 'table', '[401]', 'column', 'fksj1'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fkje2')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fkje2'

end


execute sp_addextendedproperty 'MS_Description', 
   'fkje2',
   'user', 'dbo', 'table', '[401]', 'column', 'fkje2'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fksj2')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fksj2'

end


execute sp_addextendedproperty 'MS_Description', 
   'fksj2',
   'user', 'dbo', 'table', '[401]', 'column', 'fksj2'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fkje3')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fkje3'

end


execute sp_addextendedproperty 'MS_Description', 
   'fkje3',
   'user', 'dbo', 'table', '[401]', 'column', 'fkje3'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fksj3')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fksj3'

end


execute sp_addextendedproperty 'MS_Description', 
   'fksj3',
   'user', 'dbo', 'table', '[401]', 'column', 'fksj3'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fkje4')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fkje4'

end


execute sp_addextendedproperty 'MS_Description', 
   'fkje4',
   'user', 'dbo', 'table', '[401]', 'column', 'fkje4'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fksj4')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fksj4'

end


execute sp_addextendedproperty 'MS_Description', 
   'fksj4',
   'user', 'dbo', 'table', '[401]', 'column', 'fksj4'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fkje5')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fkje5'

end


execute sp_addextendedproperty 'MS_Description', 
   'fkje5',
   'user', 'dbo', 'table', '[401]', 'column', 'fkje5'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fksj5')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'fksj5'

end


execute sp_addextendedproperty 'MS_Description', 
   'fksj5',
   'user', 'dbo', 'table', '[401]', 'column', 'fksj5'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dzxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'dzxj'

end


execute sp_addextendedproperty 'MS_Description', 
   '已到账(元)',
   'user', 'dbo', 'table', '[401]', 'column', 'dzxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sfdiya')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'sfdiya'

end


execute sp_addextendedproperty 'MS_Description', 
   'sfdiya',
   'user', 'dbo', 'table', '[401]', 'column', 'sfdiya'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'hkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   '还款次数',
   'user', 'dbo', 'table', '[401]', 'column', 'hkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已还款(元)',
   'user', 'dbo', 'table', '[401]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款(元)',
   'user', 'dbo', 'table', '[401]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[401]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'yhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'yhkxj',
   'user', 'dbo', 'table', '[401]', 'column', 'yhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'nhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'nhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'nhkxj',
   'user', 'dbo', 'table', '[401]', 'column', 'nhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ljhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'ljhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'ljhkxj',
   'user', 'dbo', 'table', '[401]', 'column', 'ljhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[401]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[401]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[401]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[401]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[401]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', '[401]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[401]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Zrdw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'Zrdw'

end


execute sp_addextendedproperty 'MS_Description', 
   '责任单位',
   'user', 'dbo', 'table', '[401]', 'column', 'Zrdw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Zrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'Zrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '责任人',
   'user', 'dbo', 'table', '[401]', 'column', 'Zrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Dklx')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'Dklx'

end


execute sp_addextendedproperty 'MS_Description', 
   '贷款类型',
   'user', 'dbo', 'table', '[401]', 'column', 'Dklx'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FlagReason')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'FlagReason'

end


execute sp_addextendedproperty 'MS_Description', 
   'FlagReason',
   'user', 'dbo', 'table', '[401]', 'column', 'FlagReason'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[401]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Rzcb')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[401]', 'column', 'Rzcb'

end


execute sp_addextendedproperty 'MS_Description', 
   '融资成本',
   'user', 'dbo', 'table', '[401]', 'column', 'Rzcb'
go

alter table dbo.[401]
   add constraint Pk_401Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [402]                                                 */
/*==============================================================*/
create table dbo.[402] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   cjzt                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   jkdw                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   jkxm                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   dkje                 money                null constraint DF_402_dkje default (0),
   jkrq                 datetime             null,
   lilv                 real                 null constraint DF_402_lilv default (0),
   qix                  real                 null constraint DF_402_qix default (0),
   bxhj                 money                null constraint DF_402_bxhj default (0),
   ydhkrq               datetime             null,
   hkcs                 smallint             null constraint DF_402_hkcs default (0),
   yfje                 money                null constraint DF_402_yfje default (0.00),
   sqk                  money                null constraint DF_402_sqk default (0.00),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   yhkxj                money                null constraint DF_402_yhkxj default (0),
   nhkxj                money                null constraint DF_402_nhkxj default (0),
   ljhkxj               money                null constraint DF_402_ljhkxj default (0),
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__402__lrsj__5DABBF2A default getdate(),
   flag                 int                  null constraint DF__402__flag__5E9FE363 default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   JkpzN                varchar(4)           collate Chinese_PRC_CI_AS null,
   JkpzY                varchar(2)           collate Chinese_PRC_CI_AS null,
   Jkpzbh               varchar(3)           collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[402]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[402]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '402其他单位借款', 
   'user', 'dbo', 'table', '[402]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[402]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', '[402]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'cjzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'cjzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '承借主体(出借人)',
   'user', 'dbo', 'table', '[402]', 'column', 'cjzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkdw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'jkdw'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款单位(借款人)',
   'user', 'dbo', 'table', '[402]', 'column', 'jkdw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkxm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'jkxm'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款项目',
   'user', 'dbo', 'table', '[402]', 'column', 'jkxm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dkje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'dkje'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款金额(元)',
   'user', 'dbo', 'table', '[402]', 'column', 'dkje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkrq')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'jkrq'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款日期',
   'user', 'dbo', 'table', '[402]', 'column', 'jkrq'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lilv')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'lilv'

end


execute sp_addextendedproperty 'MS_Description', 
   '年利率(%)',
   'user', 'dbo', 'table', '[402]', 'column', 'lilv'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'qix')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'qix'

end


execute sp_addextendedproperty 'MS_Description', 
   '期限(月)',
   'user', 'dbo', 'table', '[402]', 'column', 'qix'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bxhj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'bxhj'

end


execute sp_addextendedproperty 'MS_Description', 
   'bxhj',
   'user', 'dbo', 'table', '[402]', 'column', 'bxhj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydhkrq')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'ydhkrq'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydhkrq',
   'user', 'dbo', 'table', '[402]', 'column', 'ydhkrq'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'hkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   '还款次数',
   'user', 'dbo', 'table', '[402]', 'column', 'hkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   'yfje',
   'user', 'dbo', 'table', '[402]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款(元)',
   'user', 'dbo', 'table', '[402]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[402]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'yhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'yhkxj',
   'user', 'dbo', 'table', '[402]', 'column', 'yhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'nhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'nhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'nhkxj',
   'user', 'dbo', 'table', '[402]', 'column', 'nhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ljhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'ljhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'ljhkxj',
   'user', 'dbo', 'table', '[402]', 'column', 'ljhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[402]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[402]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[402]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[402]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', '[402]', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', '[402]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[402]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JkpzN')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'JkpzN'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证(年)',
   'user', 'dbo', 'table', '[402]', 'column', 'JkpzN'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JkpzY')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'JkpzY'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证(月)',
   'user', 'dbo', 'table', '[402]', 'column', 'JkpzY'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[402]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Jkpzbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[402]', 'column', 'Jkpzbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证编号',
   'user', 'dbo', 'table', '[402]', 'column', 'Jkpzbh'
go

alter table dbo.[402]
   add constraint Pk_402Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [403]                                                 */
/*==============================================================*/
create table dbo.[403] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   yfje                 money                null constraint DF_403_yfje default (0.00),
   xmmc                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   sj                   datetime             null,
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   hkcs                 int                  null constraint DF_403_hkcs default (0),
   yhk                  money                null constraint DF_403_yhk default (0),
   sqk                  money                null constraint DF_403_sqk default (0),
   yhkxj                money                null constraint DF_403_yhkxj default (0),
   nhkxj                money                null constraint DF_403_nhkxj default (0),
   ljhkxj               money                null constraint DF_403_ljhkxj default (0),
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__403__lrsj__5F94079C default getdate(),
   flag                 int                  null constraint DF__403__flag__60882BD5 default (0),
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   JkpzN                varchar(4)           collate Chinese_PRC_CI_AS null,
   JkpzY                varchar(2)           collate Chinese_PRC_CI_AS null,
   Jkpzbh               varchar(3)           collate Chinese_PRC_CI_AS null,
   cjzt                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   jkdw                 nvarchar(40)         collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[403]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[403]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '403盐化引资款', 
   'user', 'dbo', 'table', '[403]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[403]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', '[403]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付金额',
   'user', 'dbo', 'table', '[403]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'xmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目名称',
   'user', 'dbo', 'table', '[403]', 'column', 'xmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'sj'

end


execute sp_addextendedproperty 'MS_Description', 
   '时间',
   'user', 'dbo', 'table', '[403]', 'column', 'sj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[403]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'hkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   '还款次数',
   'user', 'dbo', 'table', '[403]', 'column', 'hkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yhk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'yhk'

end


execute sp_addextendedproperty 'MS_Description', 
   '已还款',
   'user', 'dbo', 'table', '[403]', 'column', 'yhk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', '[403]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'yhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'yhkxj',
   'user', 'dbo', 'table', '[403]', 'column', 'yhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'nhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'nhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'nhkxj',
   'user', 'dbo', 'table', '[403]', 'column', 'nhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ljhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'ljhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'ljhkxj',
   'user', 'dbo', 'table', '[403]', 'column', 'ljhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[403]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[403]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[403]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[403]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', '[403]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[403]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JkpzN')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'JkpzN'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证(年)',
   'user', 'dbo', 'table', '[403]', 'column', 'JkpzN'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JkpzY')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'JkpzY'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证(月)',
   'user', 'dbo', 'table', '[403]', 'column', 'JkpzY'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Jkpzbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'Jkpzbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款凭证编号',
   'user', 'dbo', 'table', '[403]', 'column', 'Jkpzbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'cjzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'cjzt'

end


execute sp_addextendedproperty 'MS_Description', 
   'cjzt',
   'user', 'dbo', 'table', '[403]', 'column', 'cjzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[403]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkdw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[403]', 'column', 'jkdw'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款单位',
   'user', 'dbo', 'table', '[403]', 'column', 'jkdw'
go

alter table dbo.[403]
   add constraint Pk_403Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: [404]                                                 */
/*==============================================================*/
create table dbo.[404] (
   kmmxid               int                  identity(1, 1),
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   cjzt                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   jkdw                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   jkxm                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   dkje                 money                null constraint DF_404_dkje default (0),
   jkrq                 datetime             null,
   qix                  real                 null constraint DF_404_qix default (0),
   ydhkrq               datetime             null,
   hkcs                 smallint             null constraint DF_404_hkcs default (0),
   yfje                 money                null constraint DF_404_yfje default (0),
   sqk                  money                null constraint DF_404_sqk default (0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   yhkxj                money                null constraint DF_404_yhkxj default (0),
   nhkxj                money                null constraint DF_404_nhkxj default (0),
   ljhkxj               money                null constraint DF_404_ljhkxj default (0),
   xmbh                 char(11)             collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__404__lrsj__617C500E default getdate(),
   flag                 int                  null constraint DF__404__flag__62707447 default (0),
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   JkpzN                varchar(4)           collate Chinese_PRC_CI_AS null,
   JkpzY                varchar(2)           collate Chinese_PRC_CI_AS null,
   Jkpzbh               varchar(3)           collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.[404]') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', '[404]' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   '404其他应付款', 
   'user', 'dbo', 'table', '[404]'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', '[404]', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', '[404]', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'cjzt')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'cjzt'

end


execute sp_addextendedproperty 'MS_Description', 
   '承借主体',
   'user', 'dbo', 'table', '[404]', 'column', 'cjzt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkdw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'jkdw'

end


execute sp_addextendedproperty 'MS_Description', 
   '价款单位',
   'user', 'dbo', 'table', '[404]', 'column', 'jkdw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkxm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'jkxm'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款项目',
   'user', 'dbo', 'table', '[404]', 'column', 'jkxm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dkje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'dkje'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款金额（元）',
   'user', 'dbo', 'table', '[404]', 'column', 'dkje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkrq')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'jkrq'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款日期',
   'user', 'dbo', 'table', '[404]', 'column', 'jkrq'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'qix')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'qix'

end


execute sp_addextendedproperty 'MS_Description', 
   '期限(月）',
   'user', 'dbo', 'table', '[404]', 'column', 'qix'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ydhkrq')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'ydhkrq'

end


execute sp_addextendedproperty 'MS_Description', 
   'ydhkrq',
   'user', 'dbo', 'table', '[404]', 'column', 'ydhkrq'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'hkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   '还款次数',
   'user', 'dbo', 'table', '[404]', 'column', 'hkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付金额',
   'user', 'dbo', 'table', '[404]', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款（元）',
   'user', 'dbo', 'table', '[404]', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', '[404]', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'yhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'yhkxj',
   'user', 'dbo', 'table', '[404]', 'column', 'yhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'nhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'nhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'nhkxj',
   'user', 'dbo', 'table', '[404]', 'column', 'nhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ljhkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'ljhkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'ljhkxj',
   'user', 'dbo', 'table', '[404]', 'column', 'ljhkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xmbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'xmbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '项目编号',
   'user', 'dbo', 'table', '[404]', 'column', 'xmbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', '[404]', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', '[404]', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'flag')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'flag'

end


execute sp_addextendedproperty 'MS_Description', 
   '标记',
   'user', 'dbo', 'table', '[404]', 'column', 'flag'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', '[404]', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', '[404]', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JkpzN')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'JkpzN'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款凭证（年）',
   'user', 'dbo', 'table', '[404]', 'column', 'JkpzN'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'JkpzY')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'JkpzY'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款凭证（月）',
   'user', 'dbo', 'table', '[404]', 'column', 'JkpzY'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.[404]')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Jkpzbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', '[404]', 'column', 'Jkpzbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '借款凭证编号',
   'user', 'dbo', 'table', '[404]', 'column', 'Jkpzbh'
go

alter table dbo.[404]
   add constraint Pk_404Id primary key (kmmxid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: bkb                                                   */
/*==============================================================*/
create table dbo.bkb (
   bkid                 int                  identity(1, 1),
   kmmxid               int                  null,
   kmdm                 varchar(50)          collate Chinese_PRC_CI_AS null,
   bkdw                 nvarchar(8)          collate Chinese_PRC_CI_AS null,
   bkje                 money                null constraint DF_bkb_bkje default (0),
   bksy                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   bksj                 datetime             null,
   bkpzbh               varchar(4)           collate Chinese_PRC_CI_AS null,
   bkpzN                nvarchar(4)          collate Chinese_PRC_CI_AS null,
   bkpzy                nvarchar(2)          collate Chinese_PRC_CI_AS null,
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__bkb__lrsj__2A2C1B24 default getdate(),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   ShqrjkBh             char(10)             null,
   xgsj                 smalldatetime        null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.bkb') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'bkb' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'bkb拨款表', 
   'user', 'dbo', 'table', 'bkb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'bkid'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款编号',
   'user', 'dbo', 'table', 'bkb', 'column', 'bkid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目明细编号',
   'user', 'dbo', 'table', 'bkb', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目',
   'user', 'dbo', 'table', 'bkb', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkdw')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'bkdw'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款单位',
   'user', 'dbo', 'table', 'bkb', 'column', 'bkdw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'bkje'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款金额',
   'user', 'dbo', 'table', 'bkb', 'column', 'bkje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bksy')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'bksy'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款事由',
   'user', 'dbo', 'table', 'bkb', 'column', 'bksy'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bksj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'bksj'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款时间',
   'user', 'dbo', 'table', 'bkb', 'column', 'bksj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkpzbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'bkpzbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款凭证编号',
   'user', 'dbo', 'table', 'bkb', 'column', 'bkpzbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkpzN')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'bkpzN'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款凭证（年）',
   'user', 'dbo', 'table', 'bkb', 'column', 'bkpzN'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bkpzy')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'bkpzy'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款凭证（月）',
   'user', 'dbo', 'table', 'bkb', 'column', 'bkpzy'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', 'bkb', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', 'bkb', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', 'bkb', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', 'bkb', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '最后修改',
   'user', 'dbo', 'table', 'bkb', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ShqrjkBh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'ShqrjkBh'

end


execute sp_addextendedproperty 'MS_Description', 
   'ShqrjkBh',
   'user', 'dbo', 'table', 'bkb', 'column', 'ShqrjkBh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.bkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'bkb', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', 'bkb', 'column', 'xgsj'
go

alter table dbo.bkb
   add constraint PK_BKB primary key (bkid)
go

/*==============================================================*/
/* Index: Ix_Bkb_KmmxidKmdm                                     */
/*==============================================================*/
create index Ix_Bkb_KmmxidKmdm on dbo.bkb (
kmmxid ASC,
kmdm ASC
)
with (allow_page_locks= off)
on "PRIMARY"
go

/*==============================================================*/
/* Table: djhtlxb                                               */
/*==============================================================*/
create table dbo.djhtlxb (
   htlxmc               nvarchar(10)         collate Chinese_PRC_CI_AS null,
   sjlxmc               char(4)              collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.djhtlxb') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'djhtlxb' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'djhtlxb代建工程合同类型设置', 
   'user', 'dbo', 'table', 'djhtlxb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.djhtlxb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'htlxmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'djhtlxb', 'column', 'htlxmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '合同类型名称',
   'user', 'dbo', 'table', 'djhtlxb', 'column', 'htlxmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.djhtlxb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sjlxmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'djhtlxb', 'column', 'sjlxmc'

end


execute sp_addextendedproperty 'MS_Description', 
   'sjlxmc',
   'user', 'dbo', 'table', 'djhtlxb', 'column', 'sjlxmc'
go

/*==============================================================*/
/* Table: dwdmb                                                 */
/*==============================================================*/
create table dbo.dwdmb (
   dwdm                 char(4)              collate Chinese_PRC_CI_AS not null,
   dwmc                 nvarchar(40)         collate Chinese_PRC_CI_AS not null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.dwdmb') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'dwdmb' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'dwdmb单位名称管理', 
   'user', 'dbo', 'table', 'dwdmb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dwdmb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dwdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dwdmb', 'column', 'dwdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'dwdm',
   'user', 'dbo', 'table', 'dwdmb', 'column', 'dwdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dwdmb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dwmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dwdmb', 'column', 'dwmc'

end


execute sp_addextendedproperty 'MS_Description', 
   'dwmc',
   'user', 'dbo', 'table', 'dwdmb', 'column', 'dwmc'
go

alter table dbo.dwdmb
   add constraint PK_dwdmb primary key (dwdm)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: dyb                                                   */
/*==============================================================*/
create table dbo.dyb (
   dybid                int                  identity(1, 1),
   kmmxid               int                  null,
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   dylx                 nvarchar(5)          collate Chinese_PRC_CI_AS null,
   syqr                 nvarchar(20)         collate Chinese_PRC_CI_AS null,
   dyzl                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   mianj                real                 null,
   diyazh               nvarchar(20)         collate Chinese_PRC_CI_AS null,
   pgz                  money                null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.dyb') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'dyb' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'dyb抵押表', 
   'user', 'dbo', 'table', 'dyb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dyb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dybid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dyb', 'column', 'dybid'

end


execute sp_addextendedproperty 'MS_Description', 
   'dybid',
   'user', 'dbo', 'table', 'dyb', 'column', 'dybid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dyb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dyb', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', 'dyb', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dyb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dyb', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', 'dyb', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dyb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dylx')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dyb', 'column', 'dylx'

end


execute sp_addextendedproperty 'MS_Description', 
   'dylx',
   'user', 'dbo', 'table', 'dyb', 'column', 'dylx'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dyb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'syqr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dyb', 'column', 'syqr'

end


execute sp_addextendedproperty 'MS_Description', 
   'syqr',
   'user', 'dbo', 'table', 'dyb', 'column', 'syqr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dyb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dyzl')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dyb', 'column', 'dyzl'

end


execute sp_addextendedproperty 'MS_Description', 
   'dyzl',
   'user', 'dbo', 'table', 'dyb', 'column', 'dyzl'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dyb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'mianj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dyb', 'column', 'mianj'

end


execute sp_addextendedproperty 'MS_Description', 
   'mianj',
   'user', 'dbo', 'table', 'dyb', 'column', 'mianj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dyb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'diyazh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dyb', 'column', 'diyazh'

end


execute sp_addextendedproperty 'MS_Description', 
   'diyazh',
   'user', 'dbo', 'table', 'dyb', 'column', 'diyazh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.dyb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'pgz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'dyb', 'column', 'pgz'

end


execute sp_addextendedproperty 'MS_Description', 
   'pgz',
   'user', 'dbo', 'table', 'dyb', 'column', 'pgz'
go

/*==============================================================*/
/* Table: fjb                                                   */
/*==============================================================*/
create table fjb (
   fjid                 char(10)             null,
   fjbt                 char(10)             null,
   beiz                 char(10)             null,
   mogodb               char(10)             null,
   Column_5             char(10)             null,
   Column_6             char(10)             null
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('fjb') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'fjb' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'fjb附件表', 
   'user', @CurrentUser, 'table', 'fjb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('fjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fjid')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'fjb', 'column', 'fjid'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'fjid',
   'user', @CurrentUser, 'table', 'fjb', 'column', 'fjid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('fjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fjbt')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'fjb', 'column', 'fjbt'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '附件标题',
   'user', @CurrentUser, 'table', 'fjb', 'column', 'fjbt'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('fjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'fjb', 'column', 'beiz'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', @CurrentUser, 'table', 'fjb', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('fjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'mogodb')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'fjb', 'column', 'mogodb'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '芒果DB',
   'user', @CurrentUser, 'table', 'fjb', 'column', 'mogodb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('fjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Column_5')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'fjb', 'column', 'Column_5'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '前置表',
   'user', @CurrentUser, 'table', 'fjb', 'column', 'Column_5'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('fjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Column_6')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'fjb', 'column', 'Column_6'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '前置主键',
   'user', @CurrentUser, 'table', 'fjb', 'column', 'Column_6'
go

/*==============================================================*/
/* Table: hkb                                                   */
/*==============================================================*/
create table dbo.hkb (
   hkid                 int                  identity(1, 1),
   kmmxid               int                  null,
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   hkje                 money                null constraint DF_hkb_hkje default (0),
   hksj                 datetime             null,
   pzbh                 nvarchar(10)         collate Chinese_PRC_CI_AS null,
   pzN                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   pzy                  nvarchar(2)          collate Chinese_PRC_CI_AS null,
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__hkb__lrsj__2B203F5D default getdate(),
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   PDFId                varchar(50)          collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.hkb') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'hkb' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'hkb汇款表', 
   'user', 'dbo', 'table', 'hkb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hkid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'hkid'

end


execute sp_addextendedproperty 'MS_Description', 
   'hkid',
   'user', 'dbo', 'table', 'hkb', 'column', 'hkid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', 'hkb', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', 'hkb', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hkje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'hkje'

end


execute sp_addextendedproperty 'MS_Description', 
   '还款金额',
   'user', 'dbo', 'table', 'hkb', 'column', 'hkje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hksj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'hksj'

end


execute sp_addextendedproperty 'MS_Description', 
   '还款时间',
   'user', 'dbo', 'table', 'hkb', 'column', 'hksj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'pzbh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'pzbh'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证编号',
   'user', 'dbo', 'table', 'hkb', 'column', 'pzbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'pzN')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'pzN'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证（年）',
   'user', 'dbo', 'table', 'hkb', 'column', 'pzN'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'pzy')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'pzy'

end


execute sp_addextendedproperty 'MS_Description', 
   '凭证（月）',
   'user', 'dbo', 'table', 'hkb', 'column', 'pzy'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', 'hkb', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', 'hkb', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', 'hkb', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', 'hkb', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', 'hkb', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.hkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFId')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'hkb', 'column', 'PDFId'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFId',
   'user', 'dbo', 'table', 'hkb', 'column', 'PDFId'
go

/*==============================================================*/
/* Index: Ix_Hkb_KmmxidKmdm                                     */
/*==============================================================*/
create index Ix_Hkb_KmmxidKmdm on dbo.hkb (
kmmxid ASC,
kmdm ASC
)
with (allow_page_locks= off)
on "PRIMARY"
go

/*==============================================================*/
/* Table: jkb                                                   */
/*==============================================================*/
create table dbo.jkb (
   jkbid                int                  identity(1, 1),
   kmmxid               int                  null,
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   jkje                 money                null constraint DF_jkb_jkje default (0),
   jksj                 datetime             null,
   tdcrzjk              money                null constraint DF_jkb_tdcrzjk default (0),
   gytdsyjj             money                null constraint DF_jkb_gytdsyjj default (0),
   lytdkfjj             money                null constraint DF_jkb_lytdkfjj default (0),
   tdcrywf              money                null constraint DF_jkb_tdcrywf default (0),
   czlzfbzjj            money                null constraint DF_jkb_czlzfbzjj default (0),
   jyzj                 money                null constraint DF_jkb_jyzj default (0),
   slzj                 money                null constraint DF_jkb_slzj default (0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__jkb__lrsj__2C146396 default getdate(),
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.jkb') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'jkb' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'jkb缴款表', 
   'user', 'dbo', 'table', 'jkb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkbid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'jkbid'

end


execute sp_addextendedproperty 'MS_Description', 
   'jkbid',
   'user', 'dbo', 'table', 'jkb', 'column', 'jkbid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', 'jkb', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', 'jkb', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jkje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'jkje'

end


execute sp_addextendedproperty 'MS_Description', 
   '缴款金额',
   'user', 'dbo', 'table', 'jkb', 'column', 'jkje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jksj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'jksj'

end


execute sp_addextendedproperty 'MS_Description', 
   '缴款时间',
   'user', 'dbo', 'table', 'jkb', 'column', 'jksj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tdcrzjk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'tdcrzjk'

end


execute sp_addextendedproperty 'MS_Description', 
   '土地出让总价款',
   'user', 'dbo', 'table', 'jkb', 'column', 'tdcrzjk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'gytdsyjj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'gytdsyjj'

end


execute sp_addextendedproperty 'MS_Description', 
   '国有土地收益基金',
   'user', 'dbo', 'table', 'jkb', 'column', 'gytdsyjj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lytdkfjj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'lytdkfjj'

end


execute sp_addextendedproperty 'MS_Description', 
   '农业土地开发基金',
   'user', 'dbo', 'table', 'jkb', 'column', 'lytdkfjj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tdcrywf')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'tdcrywf'

end


execute sp_addextendedproperty 'MS_Description', 
   '土地出让业务费',
   'user', 'dbo', 'table', 'jkb', 'column', 'tdcrywf'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'czlzfbzjj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'czlzfbzjj'

end


execute sp_addextendedproperty 'MS_Description', 
   '城镇廉住房保障基金',
   'user', 'dbo', 'table', 'jkb', 'column', 'czlzfbzjj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'jyzj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'jyzj'

end


execute sp_addextendedproperty 'MS_Description', 
   'jyzj',
   'user', 'dbo', 'table', 'jkb', 'column', 'jyzj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'slzj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'slzj'

end


execute sp_addextendedproperty 'MS_Description', 
   'slzj',
   'user', 'dbo', 'table', 'jkb', 'column', 'slzj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', 'jkb', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', 'jkb', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', 'jkb', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', 'jkb', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.jkb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'jkb', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', 'jkb', 'column', 'xgsj'
go

/*==============================================================*/
/* Table: kmb                                                   */
/*==============================================================*/
create table dbo.kmb (
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   sjkm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   kmmc                 nvarchar(25)         collate Chinese_PRC_CI_AS null,
   kmjb                 smallint             null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.kmb') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'kmb' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'kmb科目表', 
   'user', 'dbo', 'table', 'kmb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.kmb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'kmb', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmdm',
   'user', 'dbo', 'table', 'kmb', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.kmb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sjkm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'kmb', 'column', 'sjkm'

end


execute sp_addextendedproperty 'MS_Description', 
   'sjkm',
   'user', 'dbo', 'table', 'kmb', 'column', 'sjkm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.kmb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'kmb', 'column', 'kmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目名称',
   'user', 'dbo', 'table', 'kmb', 'column', 'kmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.kmb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmjb')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'kmb', 'column', 'kmjb'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmjb',
   'user', 'dbo', 'table', 'kmb', 'column', 'kmjb'
go

/*==============================================================*/
/* Table: lxyjb                                                 */
/*==============================================================*/
create table lxyjb (
   lxyjid               int                  null,
   swbh                 varchar(20)          null,
   swrq                 datetime             null,
   wjnr                 varchar(1000)        null,
   bgje                 money                null,
   lbdw                 varchar(50)          null,
   zfxe                 money                null
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('lxyjb') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'lxyjb' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'lxyjb', 
   'user', @CurrentUser, 'table', 'lxyjb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('lxyjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lxyjid')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'lxyjid'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'lxyjid',
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'lxyjid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('lxyjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'swbh')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'swbh'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '收文编号',
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'swbh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('lxyjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'swrq')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'swrq'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '收文日期',
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'swrq'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('lxyjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'wjnr')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'wjnr'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '文件内容',
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'wjnr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('lxyjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bgje')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'bgje'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '报告金额',
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'bgje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('lxyjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lbdw')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'lbdw'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '来宾单位',
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'lbdw'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('lxyjb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'zfxe')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'zfxe'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '支付限额',
   'user', @CurrentUser, 'table', 'lxyjb', 'column', 'zfxe'
go

/*==============================================================*/
/* Table: shqrjk                                                */
/*==============================================================*/
create table dbo.shqrjk (
   shqrjkid             int                  identity(1, 1),
   kmmxid               int                  null,
   shje                 money                null constraint DF_shqrjk_shje default (0.0),
   shrq                 smalldatetime        null,
   bksy                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   beiz                 nvarchar(40)         collate Chinese_PRC_CI_AS null,
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null constraint DF__shqrjk__lrr__274FAE79 default '',
   ShqrjkBh             char(13)             collate Chinese_PRC_CI_AS not null,
   yfje                 money                null constraint DF__shqrjk__yfje__6A11960F default (0),
   fkcs                 smallint             null constraint DF__shqrjk__fkcs__6B05BA48 default (0),
   sqk                  money                null constraint DF__shqrjk__sqk__6BF9DE81 default (0),
   lrsj                 smalldatetime        null constraint DF__shqrjk__lrsj__748F2482 default getdate(),
   dycs                 int                  null constraint DF__shqrjk__dycs__7953D99F default (0),
   PDFid                char(50)             collate Chinese_PRC_CI_AS null,
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null,
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.shqrjk') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'shqrjk' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'shqrjk审核确认价款', 
   'user', 'dbo', 'table', 'shqrjk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'shqrjkid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'shqrjkid'

end


execute sp_addextendedproperty 'MS_Description', 
   'shqrjkid',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'shqrjkid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'shje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'shje'

end


execute sp_addextendedproperty 'MS_Description', 
   'shje',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'shje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'shrq')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'shrq'

end


execute sp_addextendedproperty 'MS_Description', 
   'shrq',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'shrq'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'bksy')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'bksy'

end


execute sp_addextendedproperty 'MS_Description', 
   '拨款事由',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'bksy'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ShqrjkBh')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'ShqrjkBh'

end


execute sp_addextendedproperty 'MS_Description', 
   'ShqrjkBh',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'ShqrjkBh'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yfje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'yfje'

end


execute sp_addextendedproperty 'MS_Description', 
   '已付金额',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'yfje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'fkcs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'fkcs'

end


execute sp_addextendedproperty 'MS_Description', 
   '付款次数',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'fkcs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sqk')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'sqk'

end


execute sp_addextendedproperty 'MS_Description', 
   '尚欠款',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'sqk'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'dycs')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'dycs'

end


execute sp_addextendedproperty 'MS_Description', 
   'dycs',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'dycs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PDFid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'PDFid'

end


execute sp_addextendedproperty 'MS_Description', 
   'PDFid',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'PDFid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'xgsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.shqrjk')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'shqrjk', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', 'shqrjk', 'column', 'kmdm'
go

alter table dbo.shqrjk
   add constraint PK_SHQRJK primary key (ShqrjkBh)
go

/*==============================================================*/
/* Table: tzb                                                   */
/*==============================================================*/
create table tzb (
   tzid                 char(10)             not null,
   调整金额                 char(10)             null,
   调整事由                 char(10)             null,
   kmmid                char(10)             null
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('tzb') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'tzb' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'tzb调整表', 
   'user', @CurrentUser, 'table', 'tzb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tzb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'tzid')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tzb', 'column', 'tzid'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'tzid',
   'user', @CurrentUser, 'table', 'tzb', 'column', 'tzid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tzb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = '调整金额')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tzb', 'column', '调整金额'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '调整金额',
   'user', @CurrentUser, 'table', 'tzb', 'column', '调整金额'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tzb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = '调整事由')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tzb', 'column', '调整事由'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '调整事由',
   'user', @CurrentUser, 'table', 'tzb', 'column', '调整事由'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('tzb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmid')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'tzb', 'column', 'kmmid'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'kmmid',
   'user', @CurrentUser, 'table', 'tzb', 'column', 'kmmid'
go

alter table tzb
   add constraint PK_TZB primary key (tzid)
go

/*==============================================================*/
/* Table: yhb                                                   */
/*==============================================================*/
create table dbo.yhb (
   yhm                  nvarchar(6)          collate Chinese_PRC_CI_AS null,
   mm                   nvarchar(8)          collate Chinese_PRC_CI_AS null,
   xm                   nvarchar(8)          collate Chinese_PRC_CI_AS null,
   qx                   nvarchar(3)          collate Chinese_PRC_CI_AS null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.yhb') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'yhb' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'yhb用户管理', 
   'user', 'dbo', 'table', 'yhb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'yhm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhb', 'column', 'yhm'

end


execute sp_addextendedproperty 'MS_Description', 
   '用户名',
   'user', 'dbo', 'table', 'yhb', 'column', 'yhm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'mm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhb', 'column', 'mm'

end


execute sp_addextendedproperty 'MS_Description', 
   '密码',
   'user', 'dbo', 'table', 'yhb', 'column', 'mm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhb', 'column', 'xm'

end


execute sp_addextendedproperty 'MS_Description', 
   '姓名',
   'user', 'dbo', 'table', 'yhb', 'column', 'xm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'qx')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhb', 'column', 'qx'

end


execute sp_addextendedproperty 'MS_Description', 
   '权限',
   'user', 'dbo', 'table', 'yhb', 'column', 'qx'
go

/*==============================================================*/
/* Table: yhdkhkjhb                                             */
/*==============================================================*/
create table dbo.yhdkhkjhb (
   hkjhid               int                  identity(1, 1),
   kmmxid               int                  null,
   hksj                 smalldatetime        null,
   hkje                 money                null constraint DF_yhdkhkjhb_hkje default (0),
   beiz                 nvarchar(30)         collate Chinese_PRC_CI_AS null,
   status               bit                  null constraint DF__yhdkhkjhb__statu__1530FE3E default (0),
   lrr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   lrsj                 smalldatetime        null constraint DF__yhdkhkjhb__lrsj__2D0887CF default getdate(),
   xgr                  nvarchar(4)          collate Chinese_PRC_CI_AS null,
   xgsj                 smalldatetime        null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.yhdkhkjhb') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'yhdkhkjhb' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'yhdkhkjhb银行贷款还款计划表', 
   'user', 'dbo', 'table', 'yhdkhkjhb'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhdkhkjhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hkjhid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'hkjhid'

end


execute sp_addextendedproperty 'MS_Description', 
   'hkjhid',
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'hkjhid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhdkhkjhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmxid')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'kmmxid'

end


execute sp_addextendedproperty 'MS_Description', 
   'kmmxid',
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'kmmxid'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhdkhkjhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hksj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'hksj'

end


execute sp_addextendedproperty 'MS_Description', 
   '还款时间',
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'hksj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhdkhkjhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'hkje')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'hkje'

end


execute sp_addextendedproperty 'MS_Description', 
   '还款金额',
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'hkje'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhdkhkjhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'beiz')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'beiz'

end


execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'beiz'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhdkhkjhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'status')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'status'

end


execute sp_addextendedproperty 'MS_Description', 
   'status',
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'status'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhdkhkjhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'lrr'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入人',
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'lrr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhdkhkjhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'lrsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'lrsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '录入时间',
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'lrsj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhdkhkjhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgr')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'xgr'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'xgr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.yhdkhkjhb')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'xgsj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'xgsj'

end


execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', 'dbo', 'table', 'yhdkhkjhb', 'column', 'xgsj'
go

alter table dbo.yhdkhkjhb
   add constraint PK_yhdkhkjhb primary key (hkjhid)
      on "PRIMARY"
go

/*==============================================================*/
/* Table: zbbData                                               */
/*==============================================================*/
create table dbo.zbbData (
   sjkmsx               smallint             null,
   sjkmmc               nvarchar(10)         collate Chinese_PRC_CI_AS null,
   kmmc                 nvarchar(20)         collate Chinese_PRC_CI_AS null,
   kmdm                 nvarchar(3)          collate Chinese_PRC_CI_AS null,
   ybkxj                money                null,
   Nbkxj                money                null,
   ljbkxj               money                null
)
on "PRIMARY"
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('dbo.zbbData') and minor_id = 0)
begin 
   execute sp_dropextendedproperty 'MS_Description',  
   'user', 'dbo', 'table', 'zbbData' 
 
end 


execute sp_addextendedproperty 'MS_Description',  
   'zbbData总报表', 
   'user', 'dbo', 'table', 'zbbData'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.zbbData')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sjkmsx')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'zbbData', 'column', 'sjkmsx'

end


execute sp_addextendedproperty 'MS_Description', 
   'sjkmsx',
   'user', 'dbo', 'table', 'zbbData', 'column', 'sjkmsx'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.zbbData')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'sjkmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'zbbData', 'column', 'sjkmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   'sjkmmc',
   'user', 'dbo', 'table', 'zbbData', 'column', 'sjkmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.zbbData')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmmc')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'zbbData', 'column', 'kmmc'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目名称',
   'user', 'dbo', 'table', 'zbbData', 'column', 'kmmc'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.zbbData')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'kmdm')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'zbbData', 'column', 'kmdm'

end


execute sp_addextendedproperty 'MS_Description', 
   '科目代码',
   'user', 'dbo', 'table', 'zbbData', 'column', 'kmdm'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.zbbData')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ybkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'zbbData', 'column', 'ybkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'ybkxj',
   'user', 'dbo', 'table', 'zbbData', 'column', 'ybkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.zbbData')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Nbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'zbbData', 'column', 'Nbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'Nbkxj',
   'user', 'dbo', 'table', 'zbbData', 'column', 'Nbkxj'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('dbo.zbbData')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ljbkxj')
)
begin
   execute sp_dropextendedproperty 'MS_Description', 
   'user', 'dbo', 'table', 'zbbData', 'column', 'ljbkxj'

end


execute sp_addextendedproperty 'MS_Description', 
   'ljbkxj',
   'user', 'dbo', 'table', 'zbbData', 'column', 'ljbkxj'
go

