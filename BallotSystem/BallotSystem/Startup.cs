﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BallotSystem.Startup))]
namespace BallotSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
