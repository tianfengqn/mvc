﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BallotSystem.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            string username = Request.Form["userName"].ToString();
            string password = Request.Form["password"].ToString();
            if (username == ConfigurationManager.AppSettings["UserName"].ToString() && password == ConfigurationManager.AppSettings["Password"].ToString())
            {
                return Redirect("/Home/Index");
            }
            else {
                ViewBag.msg = "用户名或密码不正确！";
                return View("Login");
            }
        }
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]  //用post必须加

        public JsonResult cq()
        {
            int startnum = Convert.ToInt16(Request.Form["startnum"]);
            int endnum = Convert.ToInt16(Request.Form["endnum"]);
            int count = Convert.ToInt16(Request.Form["count"]);
            string result1="";
            string result2 = "";
            if (startnum >= endnum)
            {
                result1 = "起始编号必须小于结束编号";
            }
            else if (count > endnum - startnum)
            {
                result1 = "抽取数量不可大于可抽取数";
            }
            else{
                int[] result=getRandomNum(count, startnum, endnum);
                result1 = string.Join(",", result);
                string str = "<table class=\"gridtable\"><tr><th>序号</th><th>抽取结果</th></tr>";
                int i=1;
                foreach(int item in result)
                {
                    str = str + string.Format("<tr><td>{0}</td><td>{1}</td></tr>",i++, item);
                }
                str = str + "</table>";
                result2 = str;
            }
            return Json(new {
                result1 = result1,
                result2 = result2
            });
        }


        public int[] getRandomNum(int num, int minValue, int maxValue)
        {
            Random ra = new Random(unchecked((int)DateTime.Now.Ticks));
            int[] arrNum = new int[num];
            int tmp = 0;
            for (int i = 0; i <= num - 1; i++)
            {
                tmp = ra.Next(minValue, maxValue); //随机取数
                arrNum[i] = getNum(arrNum, tmp, minValue, maxValue, ra); //取出值赋到数组中
            }
            return arrNum;
        }

        public int getNum(int[] arrNum, int tmp, int minValue, int maxValue, Random ra)  //嵌套调用
        {
            foreach (int item in arrNum)
            {
                if (item == tmp) //利用循环判断是否有重复
                {
                    tmp = ra.Next(minValue, maxValue); //重新随机获取。
                    tmp=getNum(arrNum, tmp, minValue, maxValue, ra);//递归:如果取出来的数字和已取得的数字有重复就重新随机获取。
                }
            }
            return tmp;
        }
    }
}